<?php	//as a rule, it used for the VM component
defined('_JEXEC') or die;

/**
 * Template for Joomla! CMS, created with Artisteer.
 * See readme.txt for more details on how to use the template.
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions.php';

// Create alias for $this object reference:
$document = $this;

// Shortcut for template base url:
$templateUrl = $document->baseurl . '/templates/' . $document->template;

Artx::load("Artx_Page");

// Initialize $view:
$view = $this->artx = new ArtxPage($this);

// Decorate component with Artisteer style:
$view->componentWrapper();

JHtml::_('behavior.framework', true);

?>
<!DOCTYPE html>
<html dir="ltr" lang="<?php echo $document->language; ?>">
<head>
    <jdoc:include type="head" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/system/css/system.css" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/system/css/general.css" />

    <!-- Created by Artisteer v4.1.0.59861 -->
    
    
    <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">

    <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.css" media="screen">
    <!--[if lte IE 7]>
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.ie7.css" media="screen" />
    <![endif]-->
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.responsive.css" media="all">
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin">
<?	if($vm_com=JRequest::getVar('option')=='com_virtuemart'):?>
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css_xtra/style.css">
    <!--[if IE]>
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css_xtra/ie.css">
    <![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css_xtra/ie8.css">
    <![endif]-->
<?	endif;
    if(JBrowser::getInstance()->getBrowser()=="mozilla"):?>
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css_xtra/ff.css">
<?  endif;  ?>
    <!--<script>if ('undefined' != typeof jQuery) document._artxJQueryBackup = jQuery;</script>
    <script src="<?php echo $templateUrl; ?>/jquery.js"></script>-->
    <script>jQuery.noConflict();</script>

    <script src="<?php echo $templateUrl; ?>/script.js"></script>
    <?php $view->includeInlineScripts() ?>
    <script>if (document._artxJQueryBackup) jQuery = document._artxJQueryBackup;</script>
    <script src="<?php echo $templateUrl; ?>/script.responsive.js"></script>
</head>
<body>
<div id="art-main">
<header class="art-header"><?php echo $view->position('position-30', 'art-nostyle'); ?>


    <div class="art-shapes">

            </div>




                
                    
</header>
<?php if ($view->containsModules('position-1', 'position-28', 'position-29')) : ?>
<nav class="art-nav">
    <div class="art-nav-inner">
    
	<?php if ($view->containsModules('position-28')) : ?>
    	<div class="art-hmenu-extra1"><?php echo $view->position('position-28'); ?></div>
    <?php endif; ?>
<?php if ($view->containsModules('position-29')) : ?>
<div class="art-hmenu-extra2"><?php echo $view->position('position-29'); ?></div>
<?php endif; ?>
<?php echo $view->position('position-1'); ?>
 
        </div>
    </nav>
<?php endif; ?>
<div class="art-sheet clearfix">
            <?php echo $view->position('position-15', 'art-nostyle'); ?>
<?php echo $view->positions(array('position-16' => 33, 'position-17' => 33, 'position-18' => 34), 'art-block'); ?>
<div class="art-layout-wrapper">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content">
<?php
  echo $view->position('position-19', 'art-nostyle');
  if ($view->containsModules('position-2'))
    echo artxPost($view->position('position-2'));
  echo $view->positions(array('position-20' => 50, 'position-21' => 50), 'art-article');
  echo $view->position('position-12', 'art-nostyle');
  echo artxPost(array('content' => '<jdoc:include type="message" />', 'classes' => ' art-messages'));
  echo '<jdoc:include type="component" />';
  echo $view->position('position-22', 'art-nostyle');
  echo $view->positions(array('position-23' => 50, 'position-24' => 50), 'art-article');
  echo $view->position('position-25', 'art-nostyle');
?>




                        </div>
                    </div>
                </div>
            </div>
<?php echo $view->positions(array('position-9' => 33, 'position-10' => 33, 'position-11' => 34), 'art-block'); ?>
<?php echo $view->position('position-26', 'art-nostyle'); ?>


    </div>
<footer class="art-footer">
  <div class="art-footer-inner">
<?php if ($view->containsModules('position-27')) : ?>
    <?php echo $view->position('position-27', 'art-nostyle'); ?>
<?php else: ?>
<table class="art-article" style="width: 100%; "><tbody><tr><td style="width: 50%; "><span style="font-size: 11px;">All images, adverts and logos are copyrighted to their respective owners.<br><span style="font-family: 'Times New Roman', serif; ">©</span>2012 Movebox emozioni da vivere. All right reserved</span></td><td style="width: 50%; text-align: right; "><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="<?php echo $document->baseurl ?>/templates/<?php echo $document->template; ?>/images/tw.jpg"><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="<?php echo $document->baseurl ?>/templates/<?php echo $document->template; ?>/images/f.jpg"><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="<?php echo $document->baseurl ?>/templates/<?php echo $document->template; ?>/images/g+.jpg"><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="<?php echo $document->baseurl ?>/templates/<?php echo $document->template; ?>/images/bo.jpg"><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="<?php echo $document->baseurl ?>/templates/<?php echo $document->template; ?>/images/rss.jpg"><br></td></tr></tbody></table><p style="text-align: right; "><br></p>
  <?php endif; ?>
</div>
</footer>

</div>

<?php echo $view->position('debug'); ?>
<script src="<?=$document->baseurl . '/templates/' . $document->template?>/xtra_javascripts/stacktrace.js"></script>
<?  $session = JFactory::getSession();
	if($test_cart=JRequest::getVar('test_cart')){
		if($test_cart=='-1')
			$session->clear('test_cart');
		else
			$session->set('test_cart',1);
	}
    
    if($session->get('test_cart')){
        // empty cart:
        if(JRequest::getVar('clear_cart')){
            $session = JFactory::getSession();
            $session->clear('vmcart', 'vm');
            echo "<h4>vmcart is clear...</h4>";
        }
    ?>

<style>
#testbox{
	background-color: lightyellow;
    box-shadow: 0 0 10px 0 #666;
    cursor: default;
    left: 0;
    max-width: 100%;
    min-height: 40px;
    max-height: 90%;
	overflow: auto;
    position: fixed;
    top: 0;
    z-index: 1000;
    width: auto;
}
#testbox > div{
    padding: 10px;
}
#testbox > div *{
}
#testbox > div
	div{
		border: solid 1px #999;
		border-radius: 3px;
		font-family:Arial, Helvetica, sans-serif;
		margin-bottom:4px;
		padding:8px;
		padding-right: 30px;
		white-space:nowrap;
	}    
	.inside{
		background:#FFF;
		display:block !important;
	}
	.inside div{
		/**/
	}
	.flat{
		background-color:#CFF;
	}
</style>
    <div id="testbox" class="test_box">
        <div style="background: #FF0; position: fixed; padding: 2px 4px; left: 0px;">
        	<a href="javascript:void()" id="handle_test" style="font-weight:bold; text-decoration:none;" title="expand/collapse informer">&uarr;&darr;</a>
        </div>
        <div id="inbox" style="display: none;">
            <h4 style="margin-left:26px;">VMCart</h4>
	<?	if($cartSession = $session->get('vmcart', 0, 'vm')){
			$cart=unserialize($cartSession);
			function loop($cart){?>
			<div class="shell">
			<?
				foreach($cart as $key=>$val){
					$style='';
					$ob=false;
					if(is_object($val)||is_array($val)){
						$style=" class=\"inside\"";
						$ob=true;
					}else
						$style=" class=\"flat\"";
					echo "<div{$style}>[<b>$key</b>] => ";
					if($ob){
						loop($val);				
					}else{
						echo $val;
					}	
					echo "</div>";
				}?>
            </div>
		<?	}
			loop($cart);
			
		}else {?><br><hr><b>Cart is empty...</b><? }?>
        </div>
    </div>
    <script>
        //console.log('SCRIPTS COMES HERE');
    (function($){
        $('#handle_test').click(function() {
            $('#testbox #inbox').slideToggle(200);
        });
		$('.inside').dblclick(function(){
			$('div',this).toggle(200);
		});
    })(jQuery);
    </script>     
<?	}?>
</body>
</html>