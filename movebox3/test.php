
<!DOCTYPE html>
<html dir="ltr" lang="en-gb">
<head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="generator" content="Joomla! - Open Source Content Management" />
  <title>Shopping cart</title>
  <link rel="stylesheet" href="/+last-message/movebox3/components/com_virtuemart/assets/css/vmsite-ltr.css" type="text/css" />
  <link rel="stylesheet" href="/+last-message/movebox3/components/com_virtuemart/assets/css/facebox.css" type="text/css" />
  <link rel="stylesheet" href="/+last-message/movebox3/components/com_virtuemart/assets/css/chosen.css" type="text/css" />
  <link rel="stylesheet" href="/+last-message/movebox3/media/com_finder/css/finder.css" type="text/css" />
  <style type="text/css">
#facebox .content {display: block !important; height: 480px !important; overflow: auto; width: 560px !important; }
  </style>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/components/com_virtuemart/assets/js/jquery.noConflict.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/components/com_virtuemart/assets/js/vmsite.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/components/com_virtuemart/assets/js/facebox.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/media/system/js/mootools-core.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/media/system/js/core.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/media/system/js/validate.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/components/com_virtuemart/assets/js/chosen.jquery.min.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/media/system/js/mootools-more.js" type="text/javascript"></script>
  <script src="/+last-message/movebox3/media/com_finder/js/autocompleter.js" type="text/javascript"></script>
  <script type="text/javascript">
 
//<![CDATA[
		jQuery( function($) {
			$("select.virtuemart_country_id").vm2front("list",{dest : "#virtuemart_state_id",ids : "214"});
		});
//]]>
		

//<![CDATA[
	jQuery(document).ready(function($) {
		$('div#full-tos').hide();
		$('a#terms-of-service').click(function(event) {
			event.preventDefault();
			$.facebox( { div: '#full-tos' }, 'my-groovy-style');
		});
	});

//]]>


//<![CDATA[
	jQuery(document).ready(function($) {
	if ( $('#STsameAsBTjs').is(':checked') ) {
				$('#output-shipto-display').hide();
			} else {
				$('#output-shipto-display').show();
			}
		$('#STsameAsBTjs').click(function(event) {
			if($(this).is(':checked')){
				$('#STsameAsBT').val('1') ;
				$('#output-shipto-display').hide();
			} else {
				$('#STsameAsBT').val('0') ;
				$('#output-shipto-display').show();
			}
		});
	});

//]]>


  </script>

    <link rel="stylesheet" href="/+last-message/movebox3/templates/system/css/system.css" />
    <link rel="stylesheet" href="/+last-message/movebox3/templates/system/css/general.css" />

    <!-- Created by Artisteer v4.1.0.59861 -->
    
    
    <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">

    <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="/+last-message/movebox3/templates/moovebox_bianco/css/template.css" media="screen">
    <!--[if lte IE 7]>
    <link rel="stylesheet" href="/+last-message/movebox3/templates/moovebox_bianco/css/template.ie7.css" media="screen" />
    <![endif]-->
    <link rel="stylesheet" href="/+last-message/movebox3/templates/moovebox_bianco/css/template.responsive.css" media="all">
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin">
    <link rel="stylesheet" href="/+last-message/movebox3/templates/moovebox_bianco/css_xtra/style.css">
    <!--[if IE]>
    <link rel="stylesheet" href="/+last-message/movebox3/templates/moovebox_bianco/css_xtra/ie.css">
    <![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/+last-message/movebox3/templates/moovebox_bianco/css_xtra/ie8.css">
    <![endif]-->
    <!--<script>if ('undefined' != typeof jQuery) document._artxJQueryBackup = jQuery;</script>
    <script src="/+last-message/movebox3/templates/moovebox_bianco/jquery.js"></script>-->
    <script>jQuery.noConflict();</script>

    <script src="/+last-message/movebox3/templates/moovebox_bianco/script.js"></script>
        <script>if (document._artxJQueryBackup) jQuery = document._artxJQueryBackup;</script>
    <script src="/+last-message/movebox3/templates/moovebox_bianco/script.responsive.js"></script>
</head>
<body>
<div id="art-main">
<header class="art-header"><!-- begin nostyle -->
<div class="art-nostyle">
<!-- begin nostyle content -->


<div class="custom"  >
	<div style="width: 960px;">
<div><a href="index.php"><img src="images/logo.png" border="0" alt="" style="float: left; margin-top: 16px;" /></a></div>
<div style="width: 100%;">
<div style="float: right; margin-top: 22px; padding-right: 10px;"><img src="images/language.png" border="0" alt="" /></div>
<div style="float: right; margin-top: 22px; padding-right: 10px;">		<div class="moduletable">
					
<script type="text/javascript">
//<![CDATA[
	window.addEvent('domready', function() {
		var value;

		// Set the input value if not already set.
		if (!document.id('mod-finder-searchword').getProperty('value')) {
			document.id('mod-finder-searchword').setProperty('value', 'Search...');
		}

		// Get the current value.
		value = document.id('mod-finder-searchword').getProperty('value');

		// If the current value equals the default value, clear it.
		document.id('mod-finder-searchword').addEvent('focus', function() {
			if (this.getProperty('value') == 'Search...') {
				this.setProperty('value', '');
			}
		});

		// If the current value is empty, set the previous value.
		document.id('mod-finder-searchword').addEvent('blur', function() {
			if (!this.getProperty('value')) {
				this.setProperty('value', value);
			}
		});

		document.id('mod-finder-searchform').addEvent('submit', function(e){
			e = new Event(e);
			e.stop();

			// Disable select boxes with no value selected.
			if (document.id('mod-finder-advanced') != null) {
				document.id('mod-finder-advanced').getElements('select').each(function(s){
					if (!s.getProperty('value')) {
						s.setProperty('disabled', 'disabled');
					}
				});
			}

			document.id('mod-finder-searchform').submit();
		});

		/*
		 * This segment of code sets up the autocompleter.
		 */
								var url = '/+last-message/movebox3/index.php?option=com_finder&task=suggestions.display&format=json&tmpl=component&lang=en';
			var ModCompleter = new Autocompleter.Request.JSON(document.id('mod-finder-searchword'), url, {'postVar': 'q'});
			});
//]]>
</script>

<form id="mod-finder-searchform" action="/+last-message/movebox3/index.php?option=com_finder&amp;view=search&amp;lang=en" method="get" class="art-search">
	<div class="finder">
		<input type="text" name="q" id="mod-finder-searchword" class="inputbox" size="25" value="" /><input class="button art-search-button  finder" type="submit" value="Go">
				<input type="hidden" name="option" value="com_finder" /><input type="hidden" name="view" value="search" /><input type="hidden" name="lang" value="en" /><input type="hidden" name="Itemid" value="0" />	</div>
</form>
		</div>
	</div>
<div style="float: right; margin-top: 22px; padding-right: 10px;"><img src="images/nverde.png" border="0" alt="" /></div>
</div>
</div></div>
<!-- end nostyle content -->
</div>
<!-- end nostyle -->


    <div class="art-shapes">

            </div>




                
                    
</header>
<nav class="art-nav">
    <div class="art-nav-inner">
    
	<ul class="art-hmenu"><li class="item-104"><a class="viaggi" href="/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=category&amp;virtuemart_category_id=1&amp;Itemid=104&amp;lang=en">VIAGGI</a></li><li class="item-106"><a class="sport">SPORT ED ESPERIENZE</a></li><li class="item-107"><a class="benessere">BENESSERE</a></li><li class="item-108"><a class="sapori">SAPORI</a></li><li class="item-109"><a class="vsapori">VIAGGI E SAPORI</a></li><li class="item-110"><a class="multiactivity">MULTIACTIVITY</a></li><li class="item-111"><a class="coupon">COUPON</a></li><li class="item-112"><a class="pacchetti">TUTTI I PACCHETTI</a></li></ul> 
        </div>
    </nav>
<div class="art-sheet clearfix">
            <div class="art-layout-wrapper">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content">
<article class="art-post art-messages"><div class="art-postcontent clearfix">
<div id="system-message-container">
</div></div></article><article class="art-post"><div class="art-postcontent clearfix"><style>
</style>    
	<ul id="stages_path">
        	<li style="opacity:1;"><span>1</span> Carrello</li>
        	<li><span>2</span> Specifiche</li>
        	<li><span>3</span> Concludi acquisto</li>
        	<li><span>4</span> Conferma pagamento</li>
        </ul>
    <div>
		<div class="width50 floatleft">
			<h1>Cart</h1>
		</div>
				<div class="width50 floatleft right">
			<a class="continue_link" href="/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=category&amp;virtuemart_category_id=1&amp;lang=en" ><span>Continue Shopping</span></a>		</div>
		<div class="clear"></div>
	</div>
<hr>    
        

<div id="login_form_box">

   <form action="/+last-message/movebox3/index.php?option=com_virtuemart&amp;lang=en" method="post" name="login" id="form-login">
        Hello Super User	<input type="submit" name="Submit" class="button art-button" value="Logout" />
        <input type="hidden" name="option" value="com_users" />
                    <input type="hidden" name="task" value="user.logout" />
                <input type="hidden" name="8e3b626f968b07aadeb9d572163006ea" value="1" />	<input type="hidden" name="return" value="LytsYXN0LW1lc3NhZ2UvbW92ZWJveDMvaW5kZXgucGhwP29wdGlvbj1jb21fdmlydHVlbWFydCZ2aWV3PWNhcnQmbW9kZT1uZXdfY2FydCZsYW5nPWVu" />
    </form>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js">
    </script>
    <br/>
    <hr>
</div>
<br/>
	<form method="post" id="checkoutForm" name="checkoutForm" action="/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=cart&amp;lang=en">
        
        <div id="initial_choices">
            <h2>Seleziona il metodo di spedizione:</h2>
            <br>
            <div id="delivery_way">
            <label>
                <input name="delivery_method" type="radio" value="digital">
              <span class="vmpayment_name">Coupon digitale</span> 
              <span class="vmpayment_description">(Digital delivery)</span> </label>
            <label>
                <input name="delivery_method" type="radio" value="real">
              <span class="vmpayment_name">Box</span> 
              <span class="vmpayment_description">(Spedizione)</span></label>
            </div>
			    
            <div id="payment_way">
                <br>
                <hr>
                <br>
                <h2>Seleziona il metodo di pagamento preferito:</h2>
                <br>
                <div class="go_inside">
                <input type="radio" name="virtuemart_paymentmethod_id" id="payment_id_2"   value="2" >
<label for="payment_id_2"><span class="vmpayment"><span class="vmpayment_name">Cash-on-delivery</span><span class="vmpayment_description">Cash-on-delivery</span></span></label>
                                    <div class="clearfix"></div>
                                    <input type="radio" name="virtuemart_paymentmethod_id" id="payment_id_1"   value="1" checked="checked">
<label for="payment_id_1"><span class="vmpayment"><span class="vmpayment_name">PayPal</span><span class="vmpayment_description">Paypal</span></span></label>
                                    <div class="clearfix"></div>
                                                    </div>
                <div><button type="button" class="psbutton" id="btn_conferma">Conferma</button></div>
                <br>
            </div>
            <br/>
            <hr>
        </div>
        
        <div id="main_payment_form">
        	
        </div>
        
	<!--        
        <div class="sectiontableentry11" style=" display: none;" id="real">
        
        
            Shipping Method: No shipment selected            <br/>
            <a href="/+last-message/movebox3/index.php?view=cart&amp;task=edit_shipment&amp;option=com_virtuemart&amp;lang=en" class="movebox_placeholder">Select shipment</a><span  class='priceColor2'></span>
        </td>
   </td>
        <br />
  Payment Method: <span class="vmpayment_name">PayPal</span><span class="vmpayment_description">Paypal</span>                <br/>
            <a href="/+last-message/movebox3/index.php?view=cart&amp;task=editpayment&amp;option=com_virtuemart&amp;lang=en" class="movebox_placeholder">Change Payment</a>        <span  class='priceColor2'>        </span></td>
    		</td>
	 </td>        
        </div>
        <div class="sectiontableentry11" style=" display: none;" id="digital">
                Payment Method: <span class="vmpayment_name">PayPal</span><span class="vmpayment_description">Paypal</span>                <br/>
        <a href="/+last-message/movebox3/index.php?view=cart&amp;task=editpayment&amp;option=com_virtuemart&amp;lang=en" class="movebox_placeholder">Change Payment</a>        <span  class='priceColor2'>        </span></td>
    		</td>
             </td>
        
                </div>
                -->
        
		
		<script>
            var sngl=null;
            if(sngl=document.getElementById("single")){
                sngl.onchange = function() {
                localStorage['single'] = document.getElementById("single").value;
                $(".movebox_placeholder").each(function (i) {
                    $(this).attr("href", $(this).attr("href") + '&sm=' + $("#single").val());});
                }
                window.onload= function(){
                    if(localStorage['single'])
                        document.getElementById("single").value = localStorage['single'];
                             var singleValues = $("#single").val();
                if(singleValues == '--Select--'){
                    $("#digital").hide("slow");
                    $("#shipping_total").hide("slow");
                    $("#real").hide("slow");
                    $("#billto-shipto").hide("slow");
                    $("#billto").hide("slow");
                    $("#total_bill_without_shipping").hide("slow");
                    $("#bill_with_shipping").hide("slow");
                  }
                
                  //var singleValues = $("#single").val();
                  if(singleValues == 'digital'){
                    $("#digital").show("slow");
                    $("#billto").show("slow");
                    $("#shipping_total").hide("slow");
                    $("#real").hide("slow");
                    $("#billto-shipto").hide("slow");
                    $("#total_bill_without_shipping").show("slow");
                    $("#bill_with_shipping").hide("slow");
                  }
                
                  if(singleValues == 'real'){
                    $("#real").show("slow");
                    $("#shipping_total").show("slow");
                    $("#billto-shipto").show("slow");
                    $("#billto").hide("slow");
                    $("#digital").hide("slow");
                    $("#bill_with_shipping").show("slow");
                    $("#total_bill_without_shipping").hide("slow");
                  }
              }
            }
        
         function displayVals() {   
             var singleValues = $("#single").val();
            if(singleValues == '--Select--'){
                $("#digital").hide("slow");
                $("#shipping_total").hide("slow");
                $("#real").hide("slow");
                $("#billto-shipto").hide("slow");
                $("#billto").hide("slow");
                
              }
            
              //var singleValues = $("#single").val();
              if(singleValues == 'digital'){
                $("#digital").show("slow");
                $("#billto").show("slow");
                $("#shipping_total").hide("slow");
                $("#real").hide("slow");
                 $("#billto-shipto").hide("slow");
              }
            
              if(singleValues == 'real'){
                $("#real").show("slow");
                $("#shipping_total").show("slow");
                 $("#billto-shipto").show("slow");
                 $("#billto").hide("slow");
                $("#digital").hide("slow");
              }
            }
         
            $("select").change(displayVals);
            displayVals();
        </script>
        <br/>
        <div class="billto-shipto" id="billto-shipto" style=" display: <?="none"?>;">
            <div class="width50 floatleft">
        
                <span><span class="vmicon vm2-billto-icon"></span>
                    Bill To</span>
                                <div class="output-billto">
                                          <span class="values vm2-email">luca.severoni@gmail.com</span>
                                                            <br class="clear"/>
                                                      <span class="values vm2-title">Mr</span>
                                                  <span class="values vm2-first_name">John</span>
                                                  <span class="values vm2-last_name">Smith</span>
                                                            <br class="clear"/>
                                                      <span class="values vm2-address_1">5th Avanue</span>
                                                            <br class="clear"/>
                                                      <span class="values vm2-zip">00000</span>
                                                  <span class="values vm2-city">new york</span>
                                                            <br class="clear"/>
                                                      <span class="values vm2-virtuemart_country_id">Italy</span>
                                                            <br class="clear"/>
                                                      <span class="values vm2-virtuemart_state_id">Agrigento</span>
                                                            <br class="clear"/>
                                                    <div class="clear"></div>
                </div>
        
                <a class="details" href="/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=user&amp;task=editaddresscart&amp;addrtype=BT&amp;lang=en">
                    Add/Edit billing address information                </a>
        
                <input type="hidden" name="billto" value="1"/>
            </div>
        
            <div class="width50 floatleft">
        
                <span><span class="vmicon vm2-shipto-icon"></span>
                    Ship To</span>
                                <div class="output-shipto">
                    Only in case shipment address is different from billing address,<br />click »Add/Edit shipment address« button below                    <div class="clear"></div>
                </div>
                                <a class="details" href="/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=user&amp;task=editaddresscart&amp;addrtype=ST&amp;virtuemart_user_id[0]=0&amp;lang=en">
                    Add/Edit shipment address                </a>
        
            </div>
        
            <div class="clear"></div>
        </div>
        <div class="billto-shipto" id="billto" style=" display: none;">
        
            <div class="width50 floatleft">
        
                <span><span class="vmicon vm2-billto-icon"></span>
                    Bill To</span>
                                <div class="output-billto">
                    <!-- span class="titles">E-Mail</span -->
                            <span class="values vm2-email">luca.severoni@gmail.com</span>
                                                            <br class="clear"/>
                                <!-- span class="titles">Title</span -->
                            <span class="values vm2-title">Mr</span>
                            <!-- span class="titles">First Name</span -->
                            <span class="values vm2-first_name">John</span>
                            <!-- span class="titles">Last Name</span -->
                            <span class="values vm2-last_name">Smith</span>
                                                            <br class="clear"/>
                                <!-- span class="titles">Address 1</span -->
                            <span class="values vm2-address_1">5th Avanue</span>
                                                            <br class="clear"/>
                                <!-- span class="titles">Zip / Postal Code</span -->
                            <span class="values vm2-zip">00000</span>
                            <!-- span class="titles">City</span -->
                            <span class="values vm2-city">new york</span>
                                                            <br class="clear"/>
                                <!-- span class="titles">Country</span -->
                            <span class="values vm2-virtuemart_country_id">Italy</span>
                                                            <br class="clear"/>
                                <!-- span class="titles">State / Province / Region</span -->
                            <span class="values vm2-virtuemart_state_id">Agrigento</span>
                                                            <br class="clear"/>
                                                    <div class="clear"></div>
                </div>
        
                <a class="details" href="/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=user&amp;task=editaddresscart&amp;type=digital&amp;addrtype=BT&amp;lang=en">
                    Add/Edit billing address information                </a>
        
                <input type="hidden" name="billto" value="1"/>
            </div>
        
            <div class="clear"></div>
        </div>
                
        <div id="summary_box">
            <div>
                <h2>Dati di fatturazione</h2>
                <!--Email@iol.it-->
                <span id="stg_email_field">
                </span>
                <span id="stg_accepted">Hai accettato le condizioni di vendita</span>
                <!--Mr Nome e Cognome-->
                <span id="stg_first_and_last_name_field">
                </span>
                <!--343433 543543-->
                <span id="stg_phone_1_field">
                </span>
                <!--Italy-->
                <span id="stg_countty">
                </span>
                <!--Bari-->
                <span id="stg_state">
                </span>
                <button type="button" id="btn_edit_payment">Aggiungi/Modifica dati di fatturazione</button>
            </div>
            <div>
                <h2>Indirizzo di spedizione</h2>
                
                <!--<div>Se l'indirizzo di spedizione è diverso 
                dall'indirizzo di fatturazione,
                clicca su "Aggiungi/Modifica indirizzo
                di spedizione"</div>-->
                <span id="sh_address_type_name_field">
                </span>
                <!--Mr Nome e Cognome-->
                <span id="sh_first_and_last_name_field">
                </span>
                <!-- -->
                <span id="sh_address_1_field">
                </span>
                <!--Italy-->
                <span id="sh_country">
                </span>
                <!--Bari-->
                <span id="sh_state">
                </span>
                <button type="button" id="btn_edit_shipment">Aggiungi/Modifica Indirizzo di spedizione</button>
            </div>
        </div>
                
        <fieldset id="cart-summary_block">
            <table class="cart-summary"	cellspacing="0"	cellpadding="0"	border="0"	width="100%">                
            	<tr>                    
                    <th align="left">Name</th>                    
                    <th align="left">SKU</th>                    
                    <th align="center" width="60px">Price: </th>
                    <th align="right" width="140px">Quantity                        / Update</th>                    
                    <th align="right" width="60px">
                                        <span  class='priceColor2'>Tax</span></th>                    <th align="right" width="60px">
                                        <span  class='priceColor2'>Discount</span></th>                    
                     <th align="right" width="70px">Total</th>
                </tr>                
                <tr valign="top" class="sectiontableentry1">                    <td align="left">
                                                <span class="cart-images">
                                                                                 </span>
                                                <a href="/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=productdetails&amp;virtuemart_product_id=1&amp;virtuemart_category_id=1&amp;lang=en" >Vacanza formato famiglia</a>                
                    </td>                    <td align="left">1</td>                    <td align="center">
                                            </td>                    <td align="right"> 
                <script type="text/javascript">
                function check1(obj) {
                // use the modulus operator '%' to see if there is a remainder
                remainder=obj.value % 1;
                quantity=obj.value;
                    if (remainder  != 0) {
                        alert('You can buy this product only in multiples of 1 pieces!!');
                        obj.value = quantity-remainder;
                        return false;
                    }
                    return true;
                }
                </script> 
        <!--
        <form action="/+last-message/movebox3/index.php?option=com_virtuemart&amp;lang=en" method="post" class="inline">                <input type="hidden" name="option" value="com_virtuemart"/>
                <input type="text" onblur="check1(this);" onclick="check1(this);" onchange="check1(this);" onsubmit="check(1this);" title="Update Quantity In Cart" class="quantity-input js-recalculate" size="3" maxlength="4" name="quantity" value="1" />
                <input type="hidden" name="view" value="cart"/>
                <input type="hidden" name="task" value="update"/>
                <input type="hidden" name="cart_virtuemart_product_id" value="1"/>
                <input type="submit" class="vmicon vm2-add_quantity_cart" name="update" title="Update Quantity In Cart" align="middle" value=" "/>
        </form>-->                        
        <a class="vmicon vm2-remove_from_cart" title="Delete Product From Cart" align="middle" href="/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=cart&amp;task=delete&amp;cart_virtuemart_product_id=1&amp;lang=en"> </a>
                    </td>                    <td align="right"><span class='priceColor2'></span></td>                    <td align="right"><span class='priceColor2'></span></td>                    <td colspan="1" align="right">
                        <div class="PricesalesPrice" style="display : block;" ><span class="PricesalesPrice" >49,00 €</span></div></td>
                </tr>
                    <!--Begin of SubTotal, Tax, Shipment, Coupon Discount and Total listing -->                <tr>                    <td colspan="4">&nbsp;</td>                    <td colspan="3">
                        <hr/>
                    </td>
                </tr>                <tr class="sectiontableentry1">                    <td colspan="4" align="right">Product prices result</td>                    <td align="right"><span  class='priceColor2'></span></td>                    <td align="right"><span  class='priceColor2'></span></td>                    <td align="right"><div class="PricesalesPrice" style="display : block;" ><span class="PricesalesPrice" >49,00 €</span></div></td>
                </tr>                <tr id="shipping_total" style="display: none;">                    <td colspan="4" align="right">Shipping Cost: </td>                    <td align="right"><span  class='priceColor2'></span></td>                    <td align="right"><span  class='priceColor2'></span></td>                    <td align="right"> </td>
                </tr>                <tr class="sectiontableentry2">                    <td colspan="4" align="left">
                    

<!--<form method="post" id="userForm" name="enterCouponCode" action="/+last-message/movebox3/index.php?option=com_virtuemart&amp;lang=en">
    <input type="text" name="coupon_code" size="20" maxlength="50" class="coupon" alt="Enter your Coupon code" value="Enter your Coupon code" onblur="if(this.value=='') this.value='Enter your Coupon code';" onfocus="if(this.value=='Enter your Coupon code') this.value='';" />
    <span class="details-button">
    <input class="details-button" type="submit" title="Save" value="Save"/>
    </span>
    <input type="hidden" name="option" value="com_virtuemart" />
    <input type="hidden" name="view" value="cart" />
    <input type="hidden" name="task" value="setcoupon" />
    <input type="hidden" name="controller" value="cart" />
</form>-->          
          			<td colspan="6" align="left">&nbsp;</td>
                </tr>                <tr>                    
                	<td colspan="4">&nbsp;</td>                    
                    <td colspan="3">
                        <hr/>
                    </td>
                </tr>                <tr class="sectiontableentry2">                    
                	<td colspan="4" align="right">Total:</td>                    
                	<td align="right"> <span  class='priceColor2'></span> </td>                    <td align="right">  </td>                    
                    <td align="right" id="bill_with_shipping" style="display: none;"><strong><div class="PricebillTotal" style="display : block;" ><span class="PricebillTotal" >49,00 €</span></div></strong></td>                    
                    <td align="right" id="total_bill_without_shipping" style="display: none;"><strong><div class="PricesalesPrice" style="display : block;" ><span class="PricesalesPrice" >49,00 €</span></div></strong></td>
                <script>
                 function displayVals2() {
                     var singleValues = $("#single").val();
                    if(singleValues == '--Select--'){
                      $("#total_bill_without_shipping").hide("slow");
                         $("#bill_with_shipping").hide("slow");
                      }
                    
                      //var singleValues = $("#single").val();
                      if(singleValues == 'digital'){
                        $("#total_bill_without_shipping").show("slow");
                         $("#bill_with_shipping").hide("slow");
                      }
                    
                      if(singleValues == 'real'){
                          $("#bill_with_shipping").show("slow");
                          $("#total_bill_without_shipping").hide("slow");
                      }
                    }
                 
                    $("select").change(displayVals2);
                    displayVals2();
                
                
                </script>
                </tr>
            </table>
        </fieldset>
        
        <div class="cart-view" id="checkout_block">
        
            <div id="checkout-advertise-box">
			            </div>        

			                
            <div class="customer-comment marginbottom15">
                <span class="comment">Notes and special requests</span><br/>
                <textarea class="customer-comment" name="customer_comment" cols="60" rows="1"></textarea>
            </div>            
            
            <div class="checkout-button-top">
    			<div id="checkout_radios">
                	<label id="register_and_checkout">
                    	<input name="checkout_choice" type="radio" value="register_and_checkout">	Register and checkout
                    </label>
                	<label id="checkout_as_guest">
                    	<input name="checkout_choice" type="radio" value="checkout_as_guest">	Checkout as guest
                    </label>
                </div>
                
				                    <label for="tosAccepted">
                        <input type="hidden" name="tosAccepted" value="0"><input class="terms-of-service" id="tosAccepted" type="checkbox" name="tosAccepted" value="1" checked="checked" />                            <div class="terms-of-service">
    
                                <a href="" class="terms-of-service" id="terms-of-service" rel="facebox"
                                   target="_blank">
                                    <span class="vmicon vm2-termsofservice-icon"></span>
                                    Click here to read terms of service and check the box to accept them.                                </a>
                                <div id="full-tos">
                                    <h2>Terms of service</h2>
                                                                    </div>
    
                            </div>
                                    </label>
	
 						<a class="vm-button-correct" href="javascript:document.checkoutForm.submit();" ><span>Check Out Now</span></a>            </div>
			                
            <input type="hidden" name="option" value="com_virtuemart"/>
            <input type="hidden" name="view" value="cart"/>
            <input type="hidden" name="controller" value="cart">
            <input type="hidden" name="task" value="confirmAll" />
                
            <input type="hidden" id="STsameAsBT" name="STsameAsBT" value="0"/>
		<input type="hidden" name="8e3b626f968b07aadeb9d572163006ea" value="1" />	
        </div>
	
    </form>
<script src="http://localhost/+last-message/movebox3/components/com_virtuemart/views/cart/default.js"></script>
								</div>
        					</article>
                        </div>
                    </div>
                </div>
            </div>


    </div>
<footer class="art-footer">
  <div class="art-footer-inner">
<table class="art-article" style="width: 100%; "><tbody><tr><td style="width: 50%; "><span style="font-size: 11px;">All images, adverts and logos are copyrighted to their respective owners.<br><span style="font-family: 'Times New Roman', serif; ">©</span>2012 Movebox emozioni da vivere. All right reserved</span></td><td style="width: 50%; text-align: right; "><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="/+last-message/movebox3/templates/moovebox_bianco/images/tw.jpg"><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="/+last-message/movebox3/templates/moovebox_bianco/images/f.jpg"><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="/+last-message/movebox3/templates/moovebox_bianco/images/g+.jpg"><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="/+last-message/movebox3/templates/moovebox_bianco/images/bo.jpg"><img width="32" height="32" style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: rgb(191, 191, 191); border-right-color: rgb(191, 191, 191); border-bottom-color: rgb(191, 191, 191); border-left-color: rgb(191, 191, 191); " alt="" class="art-lightbox" src="/+last-message/movebox3/templates/moovebox_bianco/images/rss.jpg"><br></td></tr></tbody></table><p style="text-align: right; "><br></p>
  </div>
</footer>

</div>
</body>
</html>