<?
function fillCell($cart_data=false,$default=false){ 
	echo $cart_data;
	//echo ($default&&!$cart_data)? $default:$cart_data;
}
function selectStates($country_name,$states_id,$states_name,$sel=false){
	$query = "SELECT
	#__virtuemart_countries.virtuemart_country_id,
  virtuemart_state_id,
  state_name
FROM #__virtuemart_states
  INNER JOIN #__virtuemart_countries
    ON #__virtuemart_states.virtuemart_country_id = #__virtuemart_countries.virtuemart_country_id
  WHERE country_name = '$country_name'";
  	
  	$db=JFactory::getDBO();
	$db->setQuery($query);
	$results = $db->loadAssocList();
	$options='';
	//var_dump($query,$results);
	foreach($results as $i=>$data){
		if(!$i) $country_id = $data['virtuemart_country_id'];
		$options.='<option value='.$data['virtuemart_state_id'];
			if($sel&&$sel==$data['virtuemart_state_id'])
				 $options.=' selected ';
		$options.='>' . $data['state_name'] . '</option>';
	}
$sel=<<<SEL
<option>-- Select --</option><optgroup label='$country_name'>$options</optgroup>
SEL;
	echo($sel);
}
?>
<div class="art-postcontent clearfix">
        <fieldset id="user_data">
          <h2>Add/Edit billing address information</h2>
        <?	if($vmUser->guest):?>    
            <div class="control-buttons">
                <div>
                	Please use <strong>Register And Checkout</strong> 
                    to easily get access to your order history, 
                    or use <strong>Checkout as Guest</strong>
				</div>
                <div>
            		<button class="default" type="button" onClick="goCheckout('register_and_checkout');" title="Register And Checkout">Register And Checkout</button>
<? 	$sndto=false;
	if($sndto):?>
		<h3 class="inline">VirtueMartControllerUser::registercartuser()</h3><br>
<?	endif;?>            
            		<button class="default" title="Checkout as Guest" type="button" onClick="goCheckout('checkout_as_guest');">Checkout as Guest</button>
                    <input name="checkout_choice" type="hidden" value="">
<? 	if($sndto):?>
		<h3 class="inline">VirtueMartControllerUser::savecartuser()</h3>
<?	endif;?>	
                </div>
			</div>
            <br><br>
        <?	endif;?>    
            <hr style="clear:both; margin:20px 0;">
			
        <?  //var_dump($this->cart->BT);
			if($vmUser->guest):?>    
            <span class="userfields_info">Shopper Information</span>
            <table class="adminForm user-details">
                <tbody>
                    <tr class="req">
                        	<td class="key" title="">
                            <label class="email" for="email_field">
                                E-Mail						</label>
                        	</td>
                        	<td><input type="text" id="email_field" name="email" size="30" value="<? fillCell($this->cart->BT['email'],'testUserEmail@mailtest.com');?>" class="required" maxlength="100" aria-required="true" required> 					</td>
               	  </tr>
        <?	if($vmUser->guest):?>    
                    <tr class="req">
                        	<td class="key" title="">
                            <label class="username" for="username_field">
                                Username						</label>
                        	</td>
                        	<td>
                            <input type="text" id="username_field" name="username" size="30" value="<? 
							if(!$vmUser->guest)
								fillCell($vmUser->username);
							else
								fillCell($this->cart->BT['username'],'testUserName');
							
							?>" maxlength="25"> 					</td>
               	  </tr>
                    <tr class="req">
                        	<td class="key" title="">
                            <label class="name" for="name_field">
                                Displayed Name						</label>
                        	</td>
                        	<td>
                            <input type="text" id="name_field" name="name" size="30" value="<? fillCell($this->cart->BT['name'],'TestName');?>" maxlength="25"> 					</td>
               	  </tr>
                    <tr class="req">
                        	<td class="key" title="">
                            <label class="password" for="password_field">
                                Password						</label>
                        	</td>
                        	<td>
                            <input type="password" id="password_field" name="password" size="30" class="inputbox" value="<? fillCell(null,'history');?>">
                        	</td>
               	  </tr>
                    <tr class="req">
                        	<td class="key" title="">
                            <label class="password2" for="password2_field">
                                Confirm Password						</label>
                        	</td>
                        	<td>
                            <input type="password" id="password2_field" name="password2" size="30" class="inputbox" value="<? fillCell(null,'history');?>">
                        	</td>
               	  </tr>
                    <tr class="req">
                        	<td class="key" title="">
                            <label class="agreed" for="agreed_field">
                                I agree to the Terms of Service						</label>
                        	</td>
                        	<td>
                            <input type="checkbox" name="agreed" id="agreed_field" value="1">					
                        	</td>
               	  </tr>
        <?	endif;?>          
                </tbody>
            </table>
			<br>
        <?	endif;?>    
			<span class="userfields_info">Bill To</span>
        	<table class="adminForm user-details">
            	<tbody>
                	<tr>
                    	<td height="29" class="key" title="">
                        <label class="company" for="company_field">
                            Company Name						</label>
                    	</td>
                    	<td>
                      <input type="text" id="company_field" name="company" size="30" value="<? fillCell($this->cart->BT['company'],'Test Name of user company');?>" maxlength="64"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="title" for="title_field">
                            Title						</label>
                    	</td>
                    	<td>
                        <select id="title" name="title">
    <option value="Mr">Mr</option>
    <option value="Mrs">Mrs</option>
    </select>
                    	</td>
                	</tr>
                    <tr class="req">
                    	<td class="key" title="">
                        <label class="first_name" for="first_name_field">
                            First Name						</label>
                    	</td>
                    	<td>
                        <input type="text" id="first_name_field" name="first_name" size="30" value="<? fillCell($this->cart->BT['first_name'],'Test First Name');?>" class="required" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="middle_name" for="middle_name_field">
                            Middle Name						</label>
                    	</td>
                    	<td>
                        <input type="text" id="middle_name_field" name="middle_name" size="30" value="<? fillCell($this->cart->BT['middle_name'],'Test Middle Name');?>" maxlength="32"> 					</td>
                	</tr>
                    <tr class="req">
                    	<td class="key" title="">
                        <label class="last_name" for="last_name_field">
                            Last Name						</label>
                    	</td>
                    	<td>
                        <input type="text" id="last_name_field" name="last_name" size="30" value="<? fillCell($this->cart->BT['last_name'],'TestLastName');?>" class="required" maxlength="32"> 					</td>
                	</tr>
                    <tr class="req">
                    	<td class="key" title="">
                        <label class="address_1" for="address_1_field">
                            Address 1 					</label></td>
                    	<td>
                        <input type="text" id="address_1_field" name="address_1" size="30" value="<? fillCell($this->cart->BT['address_1'],'The test first adress');?>" class="required" maxlength="64"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="address_2" for="address_2_field">
                            Address 2						</label>
                    	</td>
                    	<td>
                        <input type="text" id="address_2_field" name="address_2" size="30" value="<? fillCell($this->cart->BT['address_2'],'The test second adress');?>" maxlength="64"> 					</td>
                	</tr>
                    <tr class="req">
                    	<td class="key" title="">
                        <label class="zip" for="zip_field">
                            Zip / Postal Code						</label>
                    	</td>
                    	<td>
                        <input type="text" id="zip_field" name="zip" size="30" value="<? fillCell($this->cart->BT['zip'],'testZip');?>" class="required" maxlength="32"> 					</td>
                	</tr>
                    <tr class="req">
                    	<td class="key" title="">
                        <label class="city" for="city_field">
                            City						</label>
                    	</td>
                    	<td>
                        <input type="text" id="city_field" name="city" size="30" value="<? fillCell($this->cart->BT['city'],'Test-City-on-Test-River');?>" class="required" maxlength="32"> 					</td>
                	</tr>
                    <tr class="req">
                    	<td class="key" title="">
                        <label class="virtuemart_country_id" for="virtuemart_country_id_field">
                            Country						</label>
                    	</td>
                    	<td>
                        <select id="payment_virtuemart_country_id" name="virtuemart_country_id" class="virtuemart_country_id required">
    <option value="" selected="selected">-- Select --</option>
            <option value="105"<?	
				if((int)$this->cart->BT['virtuemart_country_id']==105):
					?> selected<? 
				endif;
			?>>Italy</option>
            <option value="222"<?	
				if((int)$this->cart->BT['virtuemart_country_id']==222):
					?> selected<? 
				endif;
			?>>United Kingdom</option>
    </select>
                    	</td>
                	</tr>
                    <tr class="req">
                    	<td class="key" title="">
                        <label class="virtuemart_state_id" for="virtuemart_state_id_field">
                            State / Province / Region						</label>
                    	</td>
                    	<td id="payment_states">
                        	<select class="inputbox multiple" id="payment_virtuemart_state_id" size="1" name="virtuemart_state_id" required="">
                      <?	if((int)$this->cart->BT['virtuemart_country_id']==105){
						  		selectStates('Italy','virtuemart_state_id','virtuemart_state_id',$this->cart->BT['virtuemart_state_id']);
					  		}else{
								selectStates('United Kingdom','virtuemart_state_id','virtuemart_state_id',$this->cart->BT['virtuemart_state_id']);		  		
							}?>
                         </select>
                        </td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="phone_1" for="phone_1_field">
                            Phone						</label>
                    	</td>
                    	<td>
                        <input type="text" id="phone_1_field" name="phone_1" size="30" value="<? fillCell($this->cart->BT['phone_1'],'+01234567890');?>" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="phone_2" for="phone_2_field">
                            Mobile Phone						</label>
                    	</td>
                    	<td>
                        <input type="text" id="phone_2_field" name="phone_2" size="30" value="<? fillCell($this->cart->BT['phone_2'],'+09876543210');?>" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="fax" for="fax_field">
                            Fax						</label>
                    	</td>
                    	<td>
                        <input type="text" id="fax_field" name="fax" size="30" value="<? fillCell($this->cart->BT['fax'],'000111222333');?>" maxlength="32"> 					</td>
                	</tr>
    
            </tbody>
            </table>
		</fieldset>
            <br>
        <fieldset id="shipping_data">
            <h2>Add/Edit shipment address</h2>
        <?	//var_dump($this->cart->ST);?>
        	<table class="adminForm user-details">
                <tbody>
                    <tr class="req">
                        <td class="key" title="">
                            <label class="shipto_address_type_name" for="shipto_address_type_name_field">
                                Address Nickname						</label>
                        </td>
                        <td>
                          <input type="text" id="shipto_address_type_name_field" name="shipto_address_type_name" size="30" value="<? fillCell($this->cart->ST['address_type_name'],'Test shipto adress type name');?>" class="required" maxlength="32" aria-required="true" required> 					</td>
                    </tr>
                    <tr>
                        <td class="key" title="">
                            <label class="shipto_company" for="shipto_company_field">
                                Company Name						</label>
                        </td>
                        <td>
                            <input type="text" id="shipto_company_field" name="shipto_company" size="30" value="<? fillCell($this->cart->ST['company'],'Test shipto company name');?>" maxlength="64"> 					</td>
                    </tr>
                    <tr class="req">
                        <td class="key" title="">
                            <label class="shipto_first_name" for="shipto_first_name_field">
                                First Name						</label>
                        </td>
                        <td>
                          <input type="text" id="shipto_first_name_field" name="shipto_first_name" size="30" value="<? fillCell($this->cart->ST['first_name'],'Test shipto First Name');?>" class="required" maxlength="32" aria-required="true" required> 					</td>
                    </tr>
                    <tr>
                        <td class="key" title="">
                            <label class="shipto_middle_name" for="shipto_middle_name_field">
                                Middle Name						</label>
                        </td>
                        <td>
                            <input type="text" id="shipto_middle_name_field" name="shipto_middle_name" size="30" value="<? fillCell($this->cart->ST['middle_name'],'Test Shipto Middle Name');?>" maxlength="32"> 					</td>
                    </tr>
                    <tr class="req">
                        <td class="key" title="">
                            <label class="shipto_last_name" for="shipto_last_name_field">
                                Last Name						</label></td>
                        <td>
                          <input type="text" id="shipto_last_name_field" name="shipto_last_name" size="30" value="<? fillCell($this->cart->ST['last_name'],'Test shipto Last Name');?>" class="required" maxlength="32" aria-required="true" required> 					</td>
                    </tr>
                    <tr class="req">
                        <td class="key" title="">
                            <label class="shipto_address_1" for="shipto_address_1_field">
                                Address 1						</label>
                        </td>
                        <td>
                          <input type="text" id="shipto_address_1_field" name="shipto_address_1" size="30" value="<? fillCell($this->cart->ST['address_1'], 'Test shipto first address 1');?>" class="required" maxlength="64" aria-required="true" required> 					</td>
                    </tr>
                    <tr>
                        <td class="key" title="">
                            <label class="shipto_address_2" for="shipto_address_2_field">
                                Address 2						</label>
                        </td>
                        <td>
                            <input type="text" id="shipto_address_2_field" name="shipto_address_2" size="30" value="<? fillCell($this->cart->ST['address_2'], 'Test shipto second address');?>" maxlength="64"> 					</td>
                    </tr>
                    <tr class="req">
                        <td class="key" title="">
                            <label class="shipto_zip" for="shipto_zip_field">
                                Zip / Postal Code						</label>
                        </td>
                        <td>
                          <input type="text" id="shipto_zip_field" name="shipto_zip" size="30" value="<? fillCell($this->cart->ST['zip'],'testShiptoZip');?>" class="required" maxlength="32" aria-required="true" required> 					</td>
                    </tr>
                    <tr class="req">
                        <td class="key" title="">
                            <label class="shipto_city" for="shipto_city_field">
                                City						</label>
                        </td>
                        <td>
                          <input type="text" id="shipto_city_field" name="shipto_city" size="30" value="<? fillCell($this->cart->ST['city'],'Test Shipto-city');?>" class="required" maxlength="32" aria-required="true" required> 					</td>
                    </tr>
                    <tr class="req">
                        <td class="key" title="">
                            <label class="shipto_virtuemart_country_id" for="shipto_virtuemart_country_id_field">
                                Country						</label>
                        </td>
                        <td>
                          <select id="shipto_virtuemart_country_id" name="shipto_virtuemart_country_id" class="virtuemart_country_id required" aria-required="true" required="required">
        <option value="" selected="selected">-- Select --</option>
                <option value="105"<?	
				if((int)$this->cart->ST['virtuemart_country_id']==105):
					?> selected<? 
				endif;
			?>>Italy</option>
                <option value="222"<?	
				if((int)$this->cart->ST['virtuemart_country_id']==222):
					?> selected<? 
				endif;
			?>>United Kingdom</option>
    </select>
                        </td>
                    </tr>
                    <tr class="req">
                        <td class="key" title="">
                            <label class="shipto_virtuemart_state_id" for="shipto_virtuemart_state_id_field">
                                State / Province / Region						</label>
                        </td>
                      <td id="shipment_states">
                      <select class="inputbox multiple" id="shipto_virtuemart_state_id" size="1" name="shipto_virtuemart_state_id" required="">
                      	<?	if((int)$this->cart->ST['virtuemart_country_id']==105){
						  		selectStates('Italy','virtuemart_state_id','virtuemart_state_id',$this->cart->ST['virtuemart_state_id']);
					  		}else{
								selectStates('United Kingdom','virtuemart_state_id','virtuemart_state_id',$this->cart->ST['virtuemart_state_id']);		  		
							}?>
                      </select>
                      </td>
                    </tr>
                    <tr>
                        <td class="key" title="">
                            <label class="shipto_phone_1" for="shipto_phone_1_field">
                                Phone						</label>
                        </td>
                        <td>
                            <input type="text" id="shipto_phone_1_field" name="shipto_phone_1" size="30" value=" <? fillCell($this->cart->ST['phone_1'],'+1234567890');?>" maxlength="32"> 					</td>
                    </tr>
                    <tr>
                        <td class="key" title="">
                            <label class="shipto_phone_2" for="shipto_phone_2_field">
                                Mobile Phone						</label>
                        </td>
                        <td>
                            <input type="text" id="shipto_phone_2_field" name="shipto_phone_2" size="30" value="<? fillCell($this->cart->ST['phone_2'],'+1212121212');?>" maxlength="32"> 					</td>
                    </tr>
                    <tr>
                        <td class="key" title="">
                            <label class="shipto_fax" for="shipto_fax_field">
                                Fax						</label>
                        </td>
                        <td>
                            <input type="text" id="shipto_fax_field" name="shipto_fax" size="30" value="<? fillCell($this->cart->ST['fax'],'+1213141516');?>" maxlength="32"> 					</td>
                    </tr>
                </tbody>
          </table>
       	  <input name="addrtype" type="hidden" value="both">
        </fieldset>
        <div id="btn_main_form_commands">
        	<button type="button" class="psbutton" id="btn_conferma_form_cancel">Annula</button>
        <?	if(!$vmUser->guest):?>    
            <button type="button" class="psbutton" id="btn_conferma_form">Conferma</button>
        <?	endif;?>    
        </div>
</div>
<script type="text/javascript">
$(function(){
	//var stats = ;
	$('select[name*="virtuemart_country_id"]')
		.bind('change', function(){
		var country=($('option:selected', this).text()=='Italy')?
			'Italy':'United Kingdom';
		$(this).parents('tr').next('tr')
			.find('td:last-child select').html(makeStates(country));
	});
});

function makeStates(country){
	var Italy = "<? selectStates('Italy','virtuemart_state_id','virtuemart_state_id');?>";
	var England = "<? selectStates('United Kingdom','virtuemart_state_id','virtuemart_state_id');?>";	
	var list=(country=='Italy')? Italy:England;
	console.log('country = '+country+', list: '+list);
	return list;
}
</script>
<script language="javascript" src="<?=JUri::base()?>components/com_virtuemart/views/cart/validate_cart_forms.js"></script>
