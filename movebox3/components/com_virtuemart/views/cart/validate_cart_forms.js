// JavaScript Document
/**
 *	Validate an element value on blur.
 */
$(function(){
	$('tr.req td:last-child >*')
		.blur( function(){
			markField(this);
	});
})
/**
 *	Mark anelement containging invalid value (or missing one). 
 */
function markField(element){
	if (!$(element).val()||$(element).val().indexOf('Select')!=-1) {
		//console.log('tag = '+element.tagName+', val = '+$(element).val());
		$(element).css({
			border:'solid 1px red',
			width: '211px',
			marginLeft: '2px'
		});  // console.info(element.tagName.toUpperCase());
		return false;
	}
	return true;
}
/**
 *	Validate all required elements within visible parent.
 */
function validateForm() {
	var errs=0;
	$('fieldset:visible tr.req td:last-child >*')
		.each( function(index,element){
			if(!markField(element))
				errs++;
		}); 
		if(errs>0){
			alert('Not all of required fields were filled');
			console.log('errs = '+errs);
			return false;
		}else{
			var eMess='';
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if($('input#email_field').size()){
				var eMail=$('input#email_field');
				var emailValue=$(eMail).val();
				if (!filter.test(emailValue)) {
					eMess='* E-mail is invalid or missed!';
					$(eMail).css('background-color','#FC0');
					errs++;
				}
			}
			if($('input#password_field').size()){
				if($('input#password_field').val()!=$('input#password2_field').val()){
					if (errs) eMess+='\n'; 
					eMess+='* Passwords are different!';
					$('input#password2_field').css('background-color','#FC0');
					errs++;
				}
			}
			if(errs>0){
				alert(eMess);
				return false;
			}
		}
	return true; 
}