// JavaScript Document
// common url starting point for ajax queries:
var requestPage='index.php';
// the names of stages' blocks:
var sections = {
    1:['cart-summary_block','checkout_block'],
    2:'initial_choices',
    3:['main_payment_form','login_form_box'],
    4:['summary_box','cart-summary_block','checkout_block']
};
// fields names for the payment section:
var pmf=['email_field','first_and_last_name_field','phone_1_field','country','state'];
// fields names for shipment seciton:
var smf=['address_type_name_field','first_and_last_name_field','address_1_field','country','state'];
//
//var preConferma = 'div.checkout-button-top label[for="tosAccepted"]';
var fldsData = {};
var fldsShData = {};
// this indicates that we are here by going through stages:
$(function(){
    // handle country select
    //------------------------------------------
	// replace radios into label to avoid their "floating"
	$('div.go_inside input[type="radio"]').each(function(index, element) {
		$(this).next().prepend(this);
    });
	//--------------------------------
	// set main objects:
	// LABELS -> RADIOBUTTONS in delivery way block
	var dwt=$('#delivery_way label');
	// payments block
	var pwt=$('#payment_way');
    // this is possible, that we don't know exactly which stage is current
    // (for example in case when we reload the page and get it appearance from cashe)
    // then we should check which of blocks are visible 
	if(!go_stage){ 
        go_stage=checkStages(false,37);
        //console.log('line 37, call checkStages(); stage = '+go_stage);
    }
    // create Big Green Button:
    handleGreenButtonAppearance(go_stage);
    if(go_stage>1) handleStagesAppearance();
	//------------------------------------------
	/*	section 2	*/
	// show payment block
	// click by radios on the stage 2 - 
	// choose delivery method:
	$(dwt).click( function(){ // click on label including radiobutton
		if(!$('#payment_way:visible').size()){
			$(pwt).show(300);
			$('label[for="payment_id_1"]').show(300);
		}
		var box=$('input[type="radio"]',this);
		// click UNCHECKED radio within the label:
		if(!$(box).attr('data-box')){ //console.log('new box');
			$('input[type="radio"]',dwt).removeAttr('data-box');
			$(box).attr('data-box','active');
			//user_data, shipping_data
			if($(box).val()=='digital'){
				$('label[for="payment_id_2"]').hide(300);
				$('#shipping_data').hide();
			}else{ // box
				$('label[for="payment_id_2"]').show(300);
				$('label[for="payment_id_1"]').show(300);
				$('#shipping_data').show();
			}
		}
        // show button anyway
        if($('#payment_way input[type="radio"]:checked').size())
            manageBtnPayWayConfirm('visible');
	});
	//------------------------------------------
	// choose payment method: 
	// LABELS -> RADIOBUTTONS in payments way block
	$('label',pwt).one('click', function(){
        manageBtnPayWayConfirm('visible');
		setPaymentMethod();
	});
	//------------------------------------------
	// GO TO section 3:
	// click on small "CONFERMA"  button (bellow the radios blocks 
    // (payment CHOICE / shipment CHOICE))
	$('#btn_conferma').click( function(){
		showMainForm(sections[2]); // show stage 3, hide stage 2
	});
	//------------------------------------------
	/*	section 3	*/
	// click "ANNULA" button (under the form for payment/shipment data input)
	$('#btn_conferma_form_cancel').click(function(){
		//$('#'+sections[2]).fadeIn(200);
		handleParticularStageAppearance(sections[2], 'fadeIn', true, 200);
        handleParticularStageAppearance(sections[3][0], 'fadeOut', true, 200);
        //$('#'+sections[3]).fadeOut(200);
        $('#'+sections[3][1]).fadeOut(200, function(){
			checkStages(2,95);
		});		
	});
    // Click by button "Register and Checkout" or "Checkout as Guest" - 
    // - send data to controller or update it:
    // SEE FUNCTION goCheckout() furhter...
	//------------------------------------------
	/*	section 4	*/
	// click on Aggiungi/Modifica dati di fatturazione button:
	$('#btn_edit_payment').click( function(){
		backToStage3(0);
	});
	// click on Aggiungi/Modifica Indirizzo di spedizione button:
	$('#btn_edit_shipment').click( function(){
		backToStage3(1);
	});
})
/**
 * Returns the current stage detecting which block is visible:
 */
function checkStages(stage, line){
	var activeStage = 0; 
    var trace = printStackTrace();
    //Output however you want!
    //alert(trace.join('\n\n'));
	if(stage){
		var ulStages=$('#stages_path li');
        // 0 - 1 - 2 - 3
        // 1   2   3   4
        //console.log('checkStages: '+stage+', passive from number: '+stage+', index: '+stage+', active from number 1, index 0 to number 4, index 3)');
		$(ulStages).slice(stage).css('opacity',0.15); // 4
		$(ulStages).slice(0,stage).css('opacity',1);  // 0,1,2,3
        //console.log('134: %ccheckStages(), %cline: '+line+', activeStage: ', 'color:blue; font-weight:bold;', 'color:brown', activeStage);
	}else{
        var strCnt;
        for(var i in sections){
            //console.log('sections['+i+'] ('+typeof(sections[i])+') = '+sections[i]);
            if(typeof(sections[i])==='object'){
                strCnt = 0;
                for(var s in sections[i]){
                    if(typeof(sections[i][s])==='function')
                        continue;
                    else if(typeof(sections[i][s])==='string'){
                        strCnt++;
                        if($('#'+sections[i][s]+':visible').size()){
                            strCnt--;
                        }
                    }
                }
                if(strCnt==0){
                    //console.log('visible: section['+i+']');                            
                    activeStage=i;
                }
            }else{
                if($('#'+sections[i]).is(':visible')){
                    //console.log('visible: section['+i+']');  
                    activeStage=i;	
                }
            }
        } //console.log('activeStage: '+activeStage);
        //console.log('166: %ccheckStages(), %cline: '+line+', activeStage: ', 'color:blue; font-weight:bold;', 'color:brown', activeStage);
        return activeStage;
    }    
}
/**
 * Set the checkout way and go to the stage4
 */
function goCheckout(checkout_way){
	if(validateForm()){
        // overwrite task:
        $('#checkoutForm input[name="task"]')
                .eq(1).val(checkout_way);
        
        /*if(checkout_way=='register_and_checkout'){
            
            $('#checkoutForm').submit();
            return false;
        }else*/
            
		//$('input[name="checkout_choice"]').val(checkout_way);
        var fShipment = $('#shipping_data:hidden');
        if($(fShipment).size()){
            var shData=$('table',fShipment); 
            $(shData).remove();
        }
        // send data....
        var goUrl = $('#checkoutForm').attr('action')+'&controller=cart&task='+checkout_way;
        // set address type:
        var addrtype = ($('#shipping_data:visible'))? 'both':'';
        $('input[name="addrtype"]').val(addrtype);
        
        var msg   = $('#checkoutForm').serialize();
        
        var nw=false;
		if (nw){ 
			console.log('Send data:\nUrl: '+goUrl+'\nmsg: '+msg);
		}else{
            //console.log('Send data:\nUrl: '+goUrl+'\nmsg: '+msg);
            $.ajax({
                type: 'POST',
                url: goUrl,
                data: msg,
                beforeSend: function() {
					// ~template~	common.js
					$('#checkoutForm').css('opacity',0.5);
				},
                success: function(data) {
                    console.log('data = ' + data);
//                    if(checkout_way=='register_and_checkout'){
//                        location.href+='&stage=3';
//                    }else{                        
                        $('#checkoutForm').css('opacity',1);
                        $('#checkoutForm input[name="task"]')
                                .eq(1).val('confirmAll');
                        $(fShipment).append(shData);
                        goLastStage(); // includes: transitTableData(), checkStages(4);
                    //}
                },
                error:  function(xhr, str){
                      alert('Sorry, but data saving has failed...');
                      console.log('responseCode: '+xhr.responseCode);
                }
            });
		}
	}
}
/**
 * Go last stage, really.
 **/
function goLastStage(){
    //for(var fld in fldsData) console.log(fld+': '+fldsData[fld]);
	//for(var fld in fldsShData) console.log(fld+': '+fldsShData[fld]);					
    //var val_form = false;
    // fill data on next stage:
    $('#'+sections[3][0]).fadeOut(200);	    
    $('#'+sections[3][1]).fadeOut(400, function(){
        handleParticularStageAppearance(sections[4], 'show', true, 200);
        var dlvType = $('input[name="delivery_method"]:checked').val();
        if(dlvType=='digital')
            $('#summary_box >div').eq(1).hide();
        else
            $('#summary_box >div').eq(1).show();        
        handleGreenButtonAppearance(4);
        checkStages(4,236);        
        transitTableData();                    
    });
}
/**
 * Go back from the last stage
 */
function backToStage3(action){
    // in the payment/shipment form :
    if(action==1) // show shipment form
        $('#shipping_data').show();
    else    // hide shipment form
        $('#shipping_data').hide();
    showMainForm(sections[4]); // show stage 3, hide stage 4
	// hide BG button:
    /*$('#checkout_block').fadeOut(200, function(){
		showMainForm(sections[4]); // show stage 3, hide stage 4
    });*/
}
/**
 * Handles the green button's appearance 
 */
function handleGreenButtonAppearance(stage){
	$('div.checkout-button-top img.conf').remove();
    $('div.checkout-button-top a.vm-button-correct').remove();
    var bg=img_tmpl_root+'templates/moovebox_bianco/images/bg_button_green_';
    //console.log('handleGreenButtonAppearance: %cstage = ', 'font-weight:bold', stage);
    bg+=(stage>3)? 'conferma':'asquista';
    bg+='.png'; //console.log('handleGreenButtonAppearance');
	$('div.checkout-button-top label[for="tosAccepted"]')
        .after('<img class="conf" src="'+bg+'" style="margin-bottom:-26px;" onclick="runConfermaHandler()">');
}
/**
 * Handle the stage state
 */
function handleParticularStageAppearance(ob, state, jq, dur) {
    // ob is object always
    var blockId;
    for(var b in ob){
        if(typeof(ob[b])==='function')
            continue; //console.log('type: '+typeof(ob));
        if(typeof(ob)==='string') //console.log('%cob content: ', 'font-weight: bold',ob);
            blockId=ob;
        else if(typeof(ob)==='object'){ //console.log('%ccontent: ','color:blue',ob[b]);
            blockId=ob[b];
        }
        if(jq) $('#'+blockId)[state](dur);
        else document.getElementById(blockId).style.display=state;
    }
}
/**
 * Comment
 */
function handleStagesAppearance(set_stage){
    // go_stage / global
    if (!set_stage) set_stage = go_stage;
    // hide or show all blocks under a particular section:
    var styleDisplay;
    for(var sectNumber in sections){ //console.log('sectNumber = '+sectNumber);
        styleDisplay=(set_stage==sectNumber)? 'block':'none';
        //console.log(' sections '+sectNumber+' set display.style: '+styleDisplay);
        handleParticularStageAppearance(sections[sectNumber],styleDisplay);    
    }
    checkStages(checkStages(false,299),299);
    // copy data from the form on the stage 3 to the blocks in the stage 4: 
    if(set_stage>3){
        transitTableData();
    }
}
/**
 * 
 */
function manageBtnPayWayConfirm(set_state){
    //console.log('manageBtnPayWayConfirm(): set '+set_state);
    if(set_state=='visible'){
        if($('#btn_conferma:hidden').size())
                $('#btn_conferma').fadeIn(200);
    }else{
        if($('#btn_conferma:visible').size())
                $('#btn_conferma').fadeOut(200);
    }
}
/**
 * Implement actions after click on BG button 
 * on first(1) or last(4) stages
 */
function runConfermaHandler() {
    //alert('runConfermaHandler');
    var chckBx=$('#tosAccepted');
    try{ //console.log($(chckBx).is(':checked')); return false;
        if(!$(chckBx).is(':checked')){ // agreement checkbox
            showWarning('agreement');
            return false;
        }else{
            var stage=checkStages(false,330);
            //console.log('stage = '+stage);
            // hide blocs under the section 1, show under the section 2
            if(stage==1){
                $('#'+sections[1][0]).fadeOut(200);
                $('#'+sections[1][1]).fadeOut(200, function(){
                    $('#'+sections[2]).fadeIn(200);				
                });
                checkStages(2,338);
                return false;					
            }else{
                //console.log('go submit!');
                $('#checkoutForm').submit(); //return false;
            }
        }
    }catch(e){
        console.log(e.message);
        return false;
    }
}
/**
 * Update the cart by set payment method
 */
function setPaymentMethod(){
	var virtuemart_paymentmethod_id=$('#payment_way input[type="radio"]:checked').val();
	/*	send data to update cart:
		-------------------------
		controller 	= cart
		view 		= cart
		task 		= setpayment
		virtuemart_paymentmethod_id = 1 ([radio].val())
	*/
	var uData=requestPage+"?option=com_virtuemart";
	uData+='&virtuemart_paymentmethod_id='+virtuemart_paymentmethod_id;
	uData+='&view=cart';
	uData+='&task=setpaymentAjax';
	uData+='&controller=cart';
	//__utma
	//vc
	//uData+='';
	
	var nw=false;
	if (nw){ 
		if(confirm('Send data:\n'+uData)) 
			window.open(uData,'test');
		else 
			console.log('url: '+uData);
	}else{
		//console.log('url: '+uData);
		$.ajax({
			type: "GET",
			url:uData, // 		
			//data: uData, /*beforeSend: function() {},*/
			success: function (data) {
				console.log('Request OK');
			},
			error: function (data) {
				alert("Sorry, the query was failed...\n"+data.result);
			}
		});
	}
}
/**
 * Show payment/shipment FORM, hide current block (passed like a param).
 */
function showMainForm(current_block){
	checkStages(3,396);
	//$('#'+sections[1]).hide();
    //handleParticularStageAppearance(sections[1], 'none');
    //'main_payment_form','login_form_box'
    handleParticularStageAppearance(sections[3], 'fadeIn', true, 200);
	/*$('#'+sections[3]).fadeIn(200, function(){ 
		$('#login_form_box').fadeIn(200);
	});
    // initial_choices
	$('#'+current_block_id).fadeOut(200);*/
    handleParticularStageAppearance(current_block, 'fadeOut', true, 200);
}
/**
 * Show message if user missed to check something important
 */
function showWarning(entity){   
    // warning block
	var wrn=$('<div/>',{
			class:'warning'
		});
	var closeWarn=$('<div/>',{
		class:'close'
	}).text('x');
	
	$(wrn).append(closeWarn);
	
	var wcontent=$('<div/>',{
		class:"content"
	});
    
	var txtWarn,pBlock;
	if(entity=='agreement'){
		txtWarn='You should agree with our terms of service.';
		pBlock=$('label[for="tosAccepted"]');
		$(wrn).css({
			bottom:'18px',
			left:'35px'
		});
	}else if(entity=='checkout'){
		txtWarn='You should choose the way of checkout.';
		//pBlock=$('#checkout_radios');
		//$('label',pBlock).css('background-color','#FCF');
		$(wrn).css({
			bottom:'auto',
			left:'auto',
			top:'-30px',
			right:'87px'
		});
	}
	$(wcontent).text(txtWarn);
	$(pBlock).append(wrn);
	
	$(wrn).css({
		display:'none'
	}).append(wcontent)
	  .show(200, function(){
		$(closeWarn).click( function(){
			this.parentNode.style.display='none';
			return false;
		});
	});
	return false;

}
/**
 * Transit data from the stage 3 to the stage 4
 */
function transitTableData(){
    // payment data
    var reqs = $('#user_data tr.req input[type="text"]');
    var reqCnt = $(reqs).size();
    $(reqs).each( function(){
       if($(this).val().length>0)
           reqCnt--;
    });
    if(!reqCnt){
        fldsData[pmf[0]]=$('#email_field').val();			
        fldsData[pmf[1]]=$('#first_name_field').val()+' '+$('#last_name_field').val();
        fldsData[pmf[2]]=$('#phone_1_field').val();
        fldsData[pmf[3]]=$('#virtuemart_country_id option:selected').text();
        var listStateId = $('#payment_virtuemart_state_id option:selected');
        if($(listStateId).text().indexOf('Select')==-1)    
            fldsData[pmf[4]]=$(listStateId).text();
        for(var p=0;p<pmf.length;p++)
            $('#stg_'+pmf[p]).text(fldsData[pmf[p]]);
        // shipment data - do it only if this block is visible:
        reqs = $('#shipping_data tr.req input[type="text"]');
        reqCnt = $(reqs).size();
        $(reqs).each( function(){
           if($(this).val().length>0)
               reqCnt--;
        });
        if(!reqCnt){
            //console.log('transit 2');
            fldsShData[smf[0]]=$('#shipto_address_type_name_field').val();
            fldsShData[smf[1]]=$('#shipto_first_name_field').val()+' '+$('#shipto_last_name_field').val();
            fldsShData[smf[2]]=$('#shipto_address_1_field').val();
            fldsShData[smf[3]]=$('#shipto_virtuemart_country_id option:selected').text();
            listStateId = $('#shipto_virtuemart_state_id option:selected');
            if($(listStateId).text().indexOf('Select')==-1)    
                fldsData[smf[4]]=$(listStateId).text();
            for(var s=0;s<smf.length;s++)
                $('#sh_'+smf[s]).text(fldsShData[smf[s]]);						
        }else{
            for(var i = 0; i<4; i++){
                fldsShData[smf[i]]=null;
            }
        }
    }
}