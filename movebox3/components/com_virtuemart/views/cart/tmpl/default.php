<?php
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
// came here by creating new cart:
if(JRequest::getVar('mode')=='new_cart')
	$new_cart=true;
/*---------------------------------------
  main blocks:  
	cart-summary_block
	initial_choices
	main_payment_form
	summary_box	*/
//---------------------------------------*/

$show_marks=false;?>
<style>
<?	if($show_marks){?>
h3.mark{
	color:red;
}
h4.mark{
	color:violet;
}
<? 	}
	if(!$new_cart){?>
#checkout_block
	a.vm-button-correct{
	background:url(<?=JUri::base();?>templates/moovebox_bianco/images/bg_button_green_conferma.png) !important;
	width:257px !important;
}
<? 	}?>
</style>    
<?	
$session = JFactory::getSession();
if($vers=JRequest::getVar('old')){
	if($vers=='-1')
		$session->clear('old_version');
	else
		$session->set('old_version',1);
}
if($session->get('old_version')):
	require_once dirname(__FILE__).'/default_old.php';
else:

$vmUser=JFactory::getUser();
//var_dump($vmUser);
vmJsApi::js ('facebox');
vmJsApi::css ('facebox');
JHtml::_ ('behavior.formvalidation');
$document = JFactory::getDocument ();
$document->addScriptDeclaration ("
//<![CDATA[
	jQuery(document).ready(function($) {
		$('div#full-tos').hide();
		$('a#terms-of-service').click(function(event) {
			event.preventDefault();
			$.facebox( { div: '#full-tos' }, 'my-groovy-style');
		});
	});

//]]>
");
$document->addScriptDeclaration ("
//<![CDATA[
	jQuery(document).ready(function($) {
	if ( $('#STsameAsBTjs').is(':checked') ) {
				$('#output-shipto-display').hide();
			} else {
				$('#output-shipto-display').show();
			}
		$('#STsameAsBTjs').click(function(event) {
			if($(this).is(':checked')){
				$('#STsameAsBT').val('1') ;
				$('#output-shipto-display').hide();
			} else {
				$('#STsameAsBT').val('0') ;
				$('#output-shipto-display').show();
			}
		});
	});

//]]>

");
$document->addStyleDeclaration ('#facebox .content {display: block !important; height: 480px !important; overflow: auto; width: 560px !important; }');
//vmdebug('car7t pricesUnformatted',$this->cart->pricesUnformatted);
//vmdebug('cart cart',$this->cart->pricesUnformatted );
?>
	<ul id="stages_path">
    <?	$stages=array(
					'Carrello',
					'Specifiche',
					'Concludi acquisto',
					'Conferma pagamento'
				);
		foreach($stages as $stg => $stage):?>
    	<li<? if(!$stg){?> style="opacity:1;"<? }?>><?='<span>'.($stg+1).'</span> '.$stage?></li>
    <?	endforeach;?>
    </ul>
    <div>
		<div class="width50 floatleft">
			<h1><?php echo JText::_ ('COM_VIRTUEMART_CART_TITLE'); ?></h1>
		</div>
		<?php if (VmConfig::get ('oncheckout_show_steps', 1) && $this->checkout_task === 'confirm') {
		vmdebug ('checkout_task', $this->checkout_task);
		echo '<div class="checkoutStep" id="checkoutStep4">' . JText::_ ('COM_VIRTUEMART_USER_FORM_CART_STEP4') . '</div>';
	} ?>
		<div class="width50 floatleft right">
			<?php // Continue Shopping Button
			if ($this->continue_link_html != '') {
				echo $this->continue_link_html;
			} ?>
		</div>
		<div class="clear"></div>
	</div>
<hr>    
    <?	if($show_marks){?>
        <h3 class="mark">
        	block 1, 4: Login form box</h3><? 
		}?>    

<div id="<?="login_form_box"?>">
<?php echo shopFunctionsF::getLoginForm ($this->cart, FALSE);?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js">
    </script>
    <br/>
    <hr>
</div>
<br/>
	<form method="post" id="checkoutForm" name="checkoutForm" action="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=cart' . $taskRoute, $this->useXHTML, $this->useSSL); ?>">
    <?	if($show_marks){?>
        <h3 class="mark">
        	block 2: Initial choice</h3><? 
		}?>    
        <div id="<?="initial_choices"?>">
            <h2>Seleziona il metodo di spedizione:</h2>
            <br>
            <div id="delivery_way">
            <label>
                <input name="delivery_method" type="radio" value="digital">
              <span class="vmpayment_name">Coupon digitale</span> 
              <span class="vmpayment_description">(Digital delivery)</span> </label>
            <label>
                <input name="delivery_method" type="radio" value="real">
              <span class="vmpayment_name">Box</span> 
              <span class="vmpayment_description">(Spedizione)</span></label>
            </div>
			<?	if($show_marks){?>
                <h4 class="mark">Payment method choice</h4>
            <?	}?>    
            <div id="<?="payment_way"?>">
                <br>
                <hr>
                <br>
                <h2>Seleziona il metodo di pagamento preferito:</h2>
                <br>
                <div class="go_inside">
                <?	//var_dump($this->found_payment_method); // value of payment methods that were found
                    //	see layout select_payment.php - this code has been taken from there:
                    if ($this->found_payment_method) {
                
                        foreach ($this->paymentplugins_payments as $paymentplugin_payments) {
                            if (is_array($paymentplugin_payments)) {
                                foreach ($paymentplugin_payments as $paymentplugin_payment) {
                                    echo($paymentplugin_payment);
                                    ?>
                                    <div class="clearfix"></div>
                                    <?
                                }
                            }
                        }
                    } else {
                     echo "<h1>".$this->payment_not_found_text."</h1>";
                    } ?>
                </div>
                <div><button type="button" class="psbutton" id="btn_conferma">Conferma</button></div>
                <br>
            </div>
            <br/>
            <hr>
        </div>
    <?	if($show_marks){?>
        <h3 class="mark"> block 3: Payment form </h3><? 
		}?>    
        <div id="<?="main_payment_form"?>">
        <?	//if($vmUser->guest) 
            require_once dirname(__FILE__).'/../user.php';?>	
        </div>
        <div class="sectiontableentry11" style=" <?="display: none;"?>" id="real">
		<?php if (!$this->cart->automaticSelectedShipment) {?>
        	Shipping Method: <?=$this->cart->cartData['shipmentName']?>
            <br/>
            <?php
            if (!empty($this->layoutName) && $this->layoutName == 'default' && !$this->cart->automaticSelectedShipment) {
                echo JHTML::_ ('link', JRoute::_ ('index.php?view=cart&task=edit_shipment', $this->useXHTML, $this->useSSL), $this->select_shipment_text, 'class="movebox_placeholder"');
            } else {
                JText::_ ('COM_VIRTUEMART_CART_SHIPPING');
            }
        } else {?>
        	Shipping Method: <? echo $this->cart->cartData['shipmentName'];
		} 
		if (VmConfig::get ('show_tax')) {?>
        <span  class='priceColor2'>
			<?=$this->currencyDisplay->createPriceDiv ('shipmentTax', '', $this->cart->pricesUnformatted['shipmentTax'], FALSE)?>
        </span>
  <?	} 
  		echo $this->currencyDisplay->createPriceDiv ('salesPriceShipment', '', $this->cart->pricesUnformatted['salesPriceShipment'], FALSE); ?> </td>
        <br />
  <?	if ($this->cart->pricesUnformatted['salesPrice']>0.0 ) { 
  			if (!$this->cart->automaticSelectedPayment) { 
				?>Payment Method: <?=$this->cart->cartData['paymentName']?>
                <br/>
            <?  if (!empty($this->layoutName) && $this->layoutName == 'default') {
                echo JHTML::_ ('link', JRoute::_ ('index.php?view=cart&task=editpayment', $this->useXHTML, $this->useSSL), $this->select_payment_text, 'class="movebox_placeholder"');
            } else {
                JText::_ ('COM_VIRTUEMART_CART_PAYMENT');
            } 
		} else {?>
        	Payment Method: <?=$this->cart->cartData['paymentName']?></td>
  <?	} 
  		if (VmConfig::get ('show_tax')) { ?>
        <span  class='priceColor2'>
			<?=$this->currencyDisplay->createPriceDiv ('paymentTax', '', $this->cart->pricesUnformatted['paymentTax'], FALSE)?>
        </span>
  <?	} 
  		echo $this->currencyDisplay->createPriceDiv ('salesPricePayment', '', $this->cart->pricesUnformatted['salesPricePayment'], FALSE); ?>        
<?	} ?>
        </div>
        <div class="sectiontableentry11" style=" <?="display: none;"?>" id="digital">
<?	if ($this->cart->pricesUnformatted['salesPrice']>0.0 ) { 
		if (!$this->cart->automaticSelectedPayment) { ?>
        	Payment Method: <?=$this->cart->cartData['paymentName']?>
                <br/>
     	<?	if (!empty($this->layoutName) && $this->layoutName == 'default') {
                echo JHTML::_ ('link', JRoute::_ ('index.php?view=cart&task=editpayment', $this->useXHTML, $this->useSSL), $this->select_payment_text, 'class="movebox_placeholder"');
            } else {
                JText::_ ('COM_VIRTUEMART_CART_PAYMENT');
            } 
		} else { ?>
        	Payment Method: <?=$this->cart->cartData['paymentName']?> </td>
  <?	} 
  		if (VmConfig::get ('show_tax')) { ?>
        <span  class='priceColor2'>
			<?=$this->currencyDisplay->createPriceDiv ('paymentTax', '', $this->cart->pricesUnformatted['paymentTax'], FALSE)?>
        </span>
  <?php }
  		echo $this->currencyDisplay->createPriceDiv ('salesPricePayment', '', $this->cart->pricesUnformatted['salesPricePayment'], FALSE);
	} ?>
        </div>
        <br/>
        <div class="billto-shipto" id="billto-shipto" style=" <?="display: none;"?>">
            <div class="width50 floatleft">
        
                <span><span class="vmicon vm2-billto-icon"></span>
                    <?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_LBL'); ?></span>
                <?php // Output Bill To Address ?>
                <div class="output-billto">
                    <?php
        
                    foreach ($this->cart->BTaddress['fields'] as $item) {
                        if (!empty($item['value'])) {
                            if ($item['name'] === 'agreed') {
                                $item['value'] = ($item['value'] === 0) ? JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_TOS_NO') : JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_TOS_YES');
                            }
                            ?>
                      <span class="values vm2<?php echo '-' . $item['name'] ?>"><?php echo $this->escape ($item['value']) ?></span>
                            <?php if ($item['name'] != 'title' and $item['name'] != 'first_name' and $item['name'] != 'middle_name' and $item['name'] != 'zip') { ?>
                                <br class="clear"/>
                                <?php
                            }
                        }
                    } ?>
                    <div class="clear"></div>
                </div>
        
                <a class="details" href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_LBL'); ?>
                </a>
        
                <input type="hidden" name="billto" value="<?php echo $this->cart->lists['billTo']; ?>"/>
            </div>
        
            <div class="width50 floatleft">
        
                <span><span class="vmicon vm2-shipto-icon"></span>
                    <?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_SHIPTO_LBL'); ?></span>
                <?php // Output Bill To Address ?>
                <div class="output-shipto">
                    <?php
                    if (empty($this->cart->STaddress['fields'])) {
                        echo JText::sprintf ('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_EXPLAIN', JText::_ ('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'));
                    } else {
                        if (!class_exists ('VmHtml')) {
                            require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'html.php');
                        }
                        echo JText::_ ('COM_VIRTUEMART_USER_FORM_ST_SAME_AS_BT');
                        echo VmHtml::checkbox ('STsameAsBTjs', $this->cart->STsameAsBT) . '<br />';
                        ?>
                        <div id="output-shipto-display">
                            <?php
                            foreach ($this->cart->STaddress['fields'] as $item) {
                                if (!empty($item['value'])) {
                                    ?>
                                    <!-- <span class="titles"><?php echo $item['title'] ?></span> -->
                                    <?php
                                    if ($item['name'] == 'first_name' || $item['name'] == 'middle_name' || $item['name'] == 'zip') {
                                        ?>
                                        <span class="values<?php echo '-' . $item['name'] ?>"><?php echo $this->escape ($item['value']) ?></span>
                                        <?php } else { ?>
                                        <span class="values"><?php echo $this->escape ($item['value']) ?></span>
                                        <br class="clear"/>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clear"></div>
                </div>
                <?php if (!isset($this->cart->lists['current_id'])) {
                $this->cart->lists['current_id'] = 0;
            } ?>
                <a class="details" href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]=' . $this->cart->lists['current_id'], $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'); ?>
                </a>
        
            </div>
        
            <div class="clear"></div>
        </div>
        <div class="billto-shipto" id="billto" style=" <?="display: none;"?>">
        
            <div class="width50 floatleft">
        
                <span><span class="vmicon vm2-billto-icon"></span>
                    <?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_LBL'); ?></span>
                <?php // Output Bill To Address ?>
                <div class="output-billto">
                    <?php
        
                    foreach ($this->cart->BTaddress['fields'] as $item) {
                        if (!empty($item['value'])) {
                            if ($item['name'] === 'agreed') {
                                $item['value'] = ($item['value'] === 0) ? JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_TOS_NO') : JText::_ ('COM_VIRTUEMART_USER_FORM_BILLTO_TOS_YES');
                            }
                            ?><!-- span class="titles"><?php echo $item['title'] ?></span -->
                            <span class="values vm2<?php echo '-' . $item['name'] ?>"><?php echo $this->escape ($item['value']) ?></span>
                            <?php if ($item['name'] != 'title' and $item['name'] != 'first_name' and $item['name'] != 'middle_name' and $item['name'] != 'zip') { ?>
                                <br class="clear"/>
                                <?php
                            }
                        }
                    } ?>
                    <div class="clear"></div>
                </div>
        
                <a class="details" href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=user&task=editaddresscart&type=digital&&addrtype=BT', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_ ('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_LBL'); ?>
                </a>
        
                <input type="hidden" name="billto" value="<?php echo $this->cart->lists['billTo']; ?>"/>
            </div>
        
            <div class="clear"></div>
        </div>
    <?	if($show_marks){?>
        <h3 class="mark">
        	block 4: Summary box</h3>
		<? 
		}?>            
        <div id="<?="summary_box"?>"<?
    if(!$new_cart) echo " style=\"display:block\";"?>>
            <div>
                <h2>Dati di fatturazione</h2>
                <!--Email@iol.it-->
                <span id="stg_email_field">
                </span>
                <span id="stg_accepted">Hai accettato le condizioni di vendita</span>
                <!--Mr Nome e Cognome-->
                <span id="stg_first_and_last_name_field">
                </span>
                <!--343433 543543-->
                <span id="stg_phone_1_field">
                </span>
                <!--Italy-->
                <span id="stg_countty">
                </span>
                <!--Bari-->
                <span id="stg_state">
                </span>
                <button type="button" id="btn_edit_payment">Aggiungi/Modifica dati di fatturazione</button>
            </div>
            <div>
                <h2>Indirizzo di spedizione</h2>
                
                <!--<div>Se l'indirizzo di spedizione è diverso 
                dall'indirizzo di fatturazione,
                clicca su "Aggiungi/Modifica indirizzo
                di spedizione"</div>-->
                <span id="sh_address_type_name_field">
                </span>
                <!--Mr Nome e Cognome-->
                <span id="sh_first_and_last_name_field">
                </span>
                <!-- -->
                <span id="sh_address_1_field">
                </span>
                <!--Italy-->
                <span id="sh_country">
                </span>
                <!--Bari-->
                <span id="sh_state">
                </span>
                <button type="button" id="btn_edit_shipment">Aggiungi/Modifica Indirizzo di spedizione</button>
            </div>
        </div>
    <?	if($show_marks){?>
        <h3 class="mark">
        	block 1, 4: Cart summary block</h3><? 
		}?>            
        <fieldset id="cart-summary_block"<?
    if(!$new_cart) echo " style=\"display:block\";"?>>
            <table class="cart-summary"	cellspacing="0"	cellpadding="0"	border="0"	width="100%">
                <tr>
                    <th align="left">
						<?=JText::_ ('COM_VIRTUEMART_CART_NAME')?>
                    </th>
                    <th align="left">
						<?=JText::_ ('COM_VIRTUEMART_CART_SKU')?>
                    </th>
                    <th align="center" width="60px">
						<?=JText::_ ('COM_VIRTUEMART_CART_PRICE')?>
                    </th>
                    <th align="right" width="140px">
						<?=JText::_ ('COM_VIRTUEMART_CART_QUANTITY')?> / <?=JText::_ ('COM_VIRTUEMART_CART_ACTION')?>
					</th>
			<?php if (VmConfig::get ('show_tax')) { ?>
                    <th align="right" width="60px">
                    	<span class="priceColor2">
							<?=JText::_ ('COM_VIRTUEMART_CART_SUBTOTAL_TAX_AMOUNT')?>
						</span>
					</th>
			<?php } ?>
                    <th align="right" width="60px">
                    	<span class="priceColor2">
							<?=JText::_ ('COM_VIRTUEMART_CART_SUBTOTAL_DISCOUNT_AMOUNT')?>
                        </span>
                    </th>
                    <th align="right" width="70px">
						<?=JText::_ ('COM_VIRTUEMART_CART_TOTAL')?>
                    </th>
                </tr>
        <?php
        $i = 1;
        // 		vmdebug('$this->cart->products',$this->cart->products);
		foreach ($this->cart->products as $pkey => $prow) {
            ?>
                <tr valign="top" class="sectiontableentry<?php echo $i ?>">
                    <td align="left">
	<?php 
			if ($prow->virtuemart_media_id) { ?>
                        <span class="cart-images">
						<?php
                            if (!empty($prow->image)) {
                                echo $prow->image->displayMediaThumb ('', FALSE);
                            }
                            ?>
                        </span>
                        <?php } ?>
                        <?php echo JHTML::link ($prow->url, $prow->product_name) . $prow->customfields; ?>
                    </td>
                    <td align="left"><?php  echo $prow->product_sku ?></td>
                    <td align="center">
                        <?php
                        // vmdebug('$this->cart->pricesUnformatted[$pkey]',$this->cart->pricesUnformatted[$pkey]['priceBeforeTax']);
                        //echo $this->currencyDisplay->createPriceDiv ('basePriceVariant', '', $this->cart->pricesUnformatted[$pkey], FALSE);
                        //echo $prow->salesPrice;
						
						$price=(int)$prow->product_price ;
						echo number_format($price,2);
                        ?>
                    </td>
                    <td align="right"><?php 
                //				$step=$prow->min_order_level; 
                                if ($prow->step_order_level)
                                    $step=$prow->step_order_level;
                                else
                                    $step=1;
                                if($step==0)
                                    $step=1;
                                $alert=JText::sprintf ('COM_VIRTUEMART_WRONG_AMOUNT_ADDED', $step);
                                ?> 
        <?='<form action="'.JRoute::_ ('index.php').'" method="post" class="inline">'?>
                <input type="text" onblur="check<?=$step?>(this);" onclick="check<?=$step?>(this);" onchange="check<?=$step?>(this);" <? 
				/*onsubmit="check<?=$step?>(this);"*/
				?> title="<?=JText::_('COM_VIRTUEMART_CART_UPDATE')?>" class="quantity-input js-recalculate" size="3" maxlength="4" name="quantity" value="<?=$prow->quantity ?>" />
                
                <input type="hidden" name="cart_virtuemart_product_id" value="<?php echo $prow->cart_item_id  ?>"/>

                <input type="button" class="vmicon vm2-add_quantity_cart" name="update" title="<?=JText::_ ('COM_VIRTUEMART_CART_UPDATE')?>" align="middle" value=" "/>
        <?='</form>'?>
                        <a class="vmicon vm2-remove_from_cart" title="<?=JText::_ ('COM_VIRTUEMART_CART_DELETE')?>" align="middle" href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=cart&task=delete&cart_virtuemart_product_id=' . $prow->cart_item_id) ?>"> </a>
                    </td>
                    <?php if (VmConfig::get ('show_tax')) { ?>
                    <td align="right">
                    	<span class="priceColor2">
							<?=$this->currencyDisplay->createPriceDiv ('taxAmount', '', $this->cart->pricesUnformatted[$pkey], FALSE, FALSE, $prow->quantity)?>
                        </span>
                    </td>
                    <?php } ?>
                    <td align="right">
                    	<span class="priceColor2">
							<?=$this->currencyDisplay->createPriceDiv ('discountAmount', '', $this->cart->pricesUnformatted[$pkey], FALSE, FALSE, $prow->quantity)?>
                        </span>
                    </td>
                    <td colspan="1" align="right">
                        <?php
                        if (VmConfig::get ('checkout_show_origprice', 1) && !empty($this->cart->pricesUnformatted[$pkey]['basePriceWithTax']) && $this->cart->pricesUnformatted[$pkey]['basePriceWithTax'] != $this->cart->pricesUnformatted[$pkey]['salesPrice']) {?>
                        <span class="line-through">
						<?=$this->currencyDisplay->createPriceDiv ('basePriceWithTax', '', $this->cart->pricesUnformatted[$pkey], TRUE, FALSE, $prow->quantity)?>
						</span>
                        <br />
						<?
                        }
                        echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted[$pkey], FALSE, FALSE, $prow->quantity) ?>
                        </td>
                </tr>
            <?php
            $i = ($i==1) ? 2 : 1;
        } ?>
        <!--Begin of SubTotal, Tax, Shipment, Coupon Discount and Total listing -->
        <?php if (VmConfig::get ('show_tax')) {
            $colspan = 3;
        } else {
            $colspan = 2;
        } ?>
                <tr>
                    <td colspan="4">&nbsp;</td>
                
                    <td colspan="<?=$colspan?>">
                        <hr/>
                    </td>
                </tr>
                <tr class="sectiontableentry1">
                    <td colspan="4" align="right"><?=JText::_ ('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></td>
                    <?php if (VmConfig::get ('show_tax')) { ?>
                    <td align="right">
						<span  class="priceColor2">
							<?=$this->currencyDisplay->createPriceDiv ('taxAmount', '', $this->cart->pricesUnformatted, FALSE)?>
                        </span>
                    </td>
                    <?php } ?>
                    <td align="right">
						<span class="priceColor2">
						<?=$this->currencyDisplay->createPriceDiv ('discountAmount', '', $this->cart->pricesUnformatted, FALSE)?>
                        </span>
                    </td>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted, FALSE) ?></td>
                </tr>
                <tr id="shipping_total" style="display:<?=' none'?>;">
                    <td colspan="4" align="right"><?=JText::_ ('Shipping Cost: '); ?></td>
                    <?php if (VmConfig::get ('show_tax')) { ?>
                    <td align="right">
						<?php echo "
						<span  class='priceColor2'>" . $this->currencyDisplay->createPriceDiv ('taxAmount', '', $this->cart->pricesUnformatted, FALSE) . "
						</span>" ?></td>
                    <?php } ?>
                    <td align="right">
						<?php echo "
						<span  class='priceColor2'>" . $this->currencyDisplay->createPriceDiv ('discountAmount', '', $this->cart->pricesUnformatted, FALSE) . "
						</span>" ?></td>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ('salesPriceShipment', '', $this->cart->pricesUnformatted['salesPriceShipment'], FALSE); ?> </td>
                </tr>
        <?php
        if (VmConfig::get ('coupons_enable')) {
            ?>
                <tr class="sectiontableentry2">
                <td colspan="4" align="left">
                    <?php if (!empty($this->layoutName) && $this->layoutName == 'default') {
                    // echo JHTML::_('link', JRoute::_('index.php?view=cart&task=edit_coupon',$this->useXHTML,$this->useSSL), JText::_('COM_VIRTUEMART_CART_EDIT_COUPON'));
                    echo $this->loadTemplate ('coupon');
                }?>
                    <?php if (!empty($this->cart->cartData['couponCode'])) { ?>
                    <?php
                    echo $this->cart->cartData['couponCode'];
                    echo $this->cart->cartData['couponDescr'] ? (' (' . $this->cart->cartData['couponDescr'] . ')') : '';
                    ?></td>
          <?php if (VmConfig::get ('show_tax')) { ?>
                        <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ('couponTax', '', $this->cart->pricesUnformatted['couponTax'], FALSE); ?> </td>
                        <?php } ?>
                    <td align="right">&nbsp;</td>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ('salesPriceCoupon', '', $this->cart->pricesUnformatted['salesPriceCoupon'], FALSE); ?> </td>
                    <?php } else { ?>
                    <td colspan="6" align="left">&nbsp;</td>
                    <?php
                }?>
                </tr>
            <?php } 
        
        foreach ($this->cart->cartData['DBTaxRulesBill'] as $rule) {
            ?>
                <tr class="sectiontableentry<?php echo $i ?>">
                    <td colspan="4" align="right"><?php echo $rule['calc_name'] ?> </td>
                
                    <?php if (VmConfig::get ('show_tax')) { ?>
                    <td align="right"></td>
                    <?php } ?>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ($rule['virtuemart_calc_id'] . 'Diff', '', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'] . 'Diff'], FALSE); ?></td>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ($rule['virtuemart_calc_id'] . 'Diff', '', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'] . 'Diff'], FALSE); ?> </td>
                </tr>
            <?php
            if ($i) {
                $i = 1;
            } else {
                $i = 0;
            }
        } 
        
        foreach ($this->cart->cartData['taxRulesBill'] as $rule) {
            ?>
                <tr class="sectiontableentry<?php echo $i ?>">
                    <td colspan="4" align="right"><?php echo $rule['calc_name'] ?> </td>
                    <?php if (VmConfig::get ('show_tax')) { ?>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ($rule['virtuemart_calc_id'] . 'Diff', '', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'] . 'Diff'], FALSE); ?> </td>
                    <?php } ?>
                    <td align="right"><?php ?> </td>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ($rule['virtuemart_calc_id'] . 'Diff', '', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'] . 'Diff'], FALSE); ?> </td>
                </tr>
            <?php
            if ($i) {
                $i = 1;
            } else {
                $i = 0;
            }
        }
        foreach ($this->cart->cartData['DATaxRulesBill'] as $rule) {
            ?>
                <tr class="sectiontableentry<?php echo $i ?>">
                    <td colspan="4" align="right"><?php echo   $rule['calc_name'] ?> </td>
                    <?php if (VmConfig::get ('show_tax')) { ?>
                    <td align="right"></td>
                    <?php } ?>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ($rule['virtuemart_calc_id'] . 'Diff', '', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'] . 'Diff'], FALSE); ?>  </td>
                    <td align="right"><?php echo $this->currencyDisplay->createPriceDiv ($rule['virtuemart_calc_id'] . 'Diff', '', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'] . 'Diff'], FALSE); ?> </td>
                </tr>
            <?php
            if ($i) {
                $i = 1;
            } else {
                $i = 0;
            }
        } ?>
                <tr>
                    <td colspan="4">&nbsp;</td>
                    <td colspan="<?php echo $colspan ?>">
                        <hr/>
                    </td>
                </tr>
                <tr class="sectiontableentry2">
                    <td colspan="4" align="right"><?=JText::_ ('COM_VIRTUEMART_CART_TOTAL')?>:</td>
                    <?php if (VmConfig::get ('show_tax')) { ?>
                    <td align="right"> 
                    	<span  class='priceColor2'>
							<?=$this->currencyDisplay->createPriceDiv ('billTaxAmount', '', $this->cart->pricesUnformatted['billTaxAmount'], FALSE)?> 
                    	</span>
                    </td>
                    <?php } ?>
                    <td align="right"> 
                    	<span  class='priceColor2'>
							<?=$this->currencyDisplay->createPriceDiv ('billDiscountAmount', '', $this->cart->pricesUnformatted['billDiscountAmount'], FALSE)?> 
                    	</span>
                    </td>
                    <td align="right" id="bill_with_shipping" style="display:<?=' none'?>;">
                    	<strong>
							<?=$this->currencyDisplay->createPriceDiv ('billTotal', '', $this->cart->pricesUnformatted['billTotal'], FALSE); ?>
                        </strong>
                    </td>
                    <td align="right" id="total_bill_without_shipping"<? //=style="display:none;"?>>
                    	<strong>
							<?=$this->currencyDisplay->createPriceDiv ('salesPrice', '', $this->cart->pricesUnformatted, FALSE) ?>
                        </strong>
                    </td>
                </tr>
        <?php
		if ($this->totalInPaymentCurrency) {
        ?>
                <tr class="sectiontableentry2">
                    <td colspan="4" align="right"><?=JText::_ ('COM_VIRTUEMART_CART_TOTAL_PAYMENT')?>:</td>
                    <?php if (VmConfig::get ('show_tax')) { ?>
                    <td align="right"></td>
                    <?php } ?>
                    <td align="right"></td>
                    <td align="right"><strong><?php echo $this->totalInPaymentCurrency;   ?></strong></td>
                </tr>
<?php	}?>
            </table>
        </fieldset>
        
        <div class="cart-view" id="checkout_block">
        <?php
        
            // This displays the pricelist MUST be done with tables, because it is also used for the emails
            echo $this->loadTemplate ('pricelist');
            if ($this->checkout_task) {
                $taskRoute = '&task=' . $this->checkout_task;
            }
            else {
                $taskRoute = '';
            }
            ?>
            <div id="checkout-advertise-box">
			<?php
            if (!empty($this->checkoutAdvertise)) {
                foreach ($this->checkoutAdvertise as $checkoutAdvertise) {
                    ?>
                    <div class="checkout-advertise">
                        <?php echo $checkoutAdvertise; ?>
                    </div>
                    <?php
                }
            }
            ?>
            </div>        

			<?	if($show_marks){?>
                <h4 class="mark">Customer comment block</h4>
            <?	}?>                
            <div class="customer-comment marginbottom15">
                <span class="comment"><?php echo JText::_ ('COM_VIRTUEMART_COMMENT_CART'); ?></span><br/>
                <textarea class="customer-comment" name="customer_comment" cols="60" rows="1"><?php echo $this->cart->customer_comment; ?></textarea>
            </div>            
            
            <div class="checkout-button-top">
				<?php // Terms Of Service Checkbox
                if (!class_exists ('VirtueMartModelUserfields')) {
                    require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'userfields.php');
                }
                $userFieldsModel = VmModel::getModel ('userfields');
                if ($userFieldsModel->getIfRequired ('agreed')) {
                    ?>
                    <label for="tosAccepted">
                        <?php
                        if (!class_exists ('VmHtml')) {
                            require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'html.php');
                        }
                        echo VmHtml::checkbox ('tosAccepted', $this->cart->tosAccepted, 1, 0, 'class="terms-of-service"');
    
                        if (VmConfig::get ('oncheckout_show_legal_info', 1)) {
                            ?>
                            <div class="terms-of-service">
    
                                <a href="<?php JRoute::_ ('index.php?option=com_virtuemart&view=vendor&layout=tos&virtuemart_vendor_id=1') ?>" class="terms-of-service" id="terms-of-service" rel="facebox"
                                   target="_blank">
                                    <span class="vmicon vm2-termsofservice-icon"></span>
                                    <?php echo JText::_ ('COM_VIRTUEMART_CART_TOS_READ_AND_ACCEPTED'); ?>
                                </a>
                                <div id="full-tos">
                                    <h2><?php echo JText::_ ('COM_VIRTUEMART_CART_TOS'); ?></h2>
                                    <?php echo $this->cart->vendor->vendor_terms_of_service; ?>
                                </div>
    
                            </div>
                <?php	} ?>
                    </label>
<? 	if($sndto):?>
		<img src="../../../../../templates/moovebox_bianco/images/bg_button_green_conferma.png" width="257" height="63" />
		<h3 class="inline">VirtueMartControllerCart::confirm()</h3>
<?	endif;?>	
 		<?php	}?>
				<?=$this->checkout_link_html?>
            </div>
			<?	if($show_marks){?>
                <h4 class="mark">Block of hidden fields comes here</h4>
            <?	}?>                
            <input type="hidden" name="option" value="com_virtuemart"/>
            <input type="hidden" name="view" value="cart"/>
            <input type="hidden" name="controller" value="cart">
            <input type="hidden" name="task" value="confirmAll" />
            <?php 
			// Continue and Checkout Button END 
			if(!$vmUser->guest){	/*?>
            <input type="hidden" name="task" value="handle_cart_data" />
		<?	*/
			}	?>    
            <input type="hidden" id="STsameAsBT" name="STsameAsBT" value="<?php echo $this->cart->STsameAsBT; ?>"/>
			<?=JHTML::_('form.token')?>	
        </div>
	</form>
<script type="text/javascript">
function check<?=$step?>(obj) {
<?	if($step):?>	
	// use the modulus operator '%' to see if there is a remainder
	remainder=obj.value % <?=$step?>;
	quantity=obj.value;
	if (remainder  != 0) {
		alert('<?php echo $alert?>!');
		obj.value = quantity-remainder;
		return false;
	}
	return true;
<?	else:?>
	console.log('no var $step found...');
<?	endif;?>	
}
</script> 
<script>
$( function(){
	// change quantity:
	$('input[name="update"]').click( function(){
		var checking=check<?=$step?>(this);
		if(checking){
			var goUrl="<?=JUri::base()?>index.php?option=com_virtuemart&view=cart&task=update&cart_virtuemart_product_id=" + $('input[name="cart_virtuemart_product_id"]').val()+"&quantity=" + $('input[name="quantity"]').val();
			//console.log('goUrl: '+goUrl);
			location.href=goUrl;
		}else{
			console.log('checking is failed...'); 
			return false;				
		}
	});
});
var img_tmpl_root = '<?=JUri::base();?>';

<?	if($go_stage=JRequest::getVar('stage')):
	?>
var go_stage=<?=(int)$go_stage?>;		
<?	else:
		if(!$new_cart):
			// mark last stage as active ?>

var go_stage=4;	
transitTableData(); // transit table data from the stage 3 to the stage 4.

	<?	else:?>
var go_stage=0;
	<?	endif;
	endif;
	// don't mix "stage" and "state", Dude! :)
	if($state=JRequest::getVar('state')):
		if($state=='removed'&&!$this->totalInPaymentCurrency):?>
var smess=$('#system-message ul li:first-child');
var smText=$(smess).html();
$(smess).html(smText+'. <span style="color:red">Il carello è vuoto.</span>');
go_stage=1;
	<?	endif;	
	endif;?>	
</script>
<script src="<?=
JUri::base()?>components/com_virtuemart/views/cart/default.js"></script>
<?
endif;
