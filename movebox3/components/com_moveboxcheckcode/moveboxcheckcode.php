<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
JHTML::stylesheet('restapi.css', JURI::root() . 'components/com_moveboxcheckcode/assets/css/');
?>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>

<div id="main_wrapper">
    <div id="code_wrapper">
        <div>
            <h2>Codice promo</h2>
        </div>
        <div style="text-align: center;">
            Prenota il tuo cofonetto <br>
            Hai un cofonetto e desideri prenotore il tuo Soggiorno? <br><br>
            Nulle di pi&ugrave; semplice! <br>
            Inserisci il tuo codice confidenziale e 9 cifre
            <div><input type="text" placeholder="Inserisci qui il tuo codice" name="code_text" id="code_text" /></div>
            <div id="error_msg" class="error">
                Inserisci il codice e premi Conferma
            </div>
            <div id="not_valid_response_div" class="error" align="center">
                Il codice NON &Egrave; VALIDO
            </div>
            <div id="verify_div"><input type="button" name="verify" id="verify" value='Conferma' class="effect7"/></div>
        </div>
    </div>
    <div id="valid_response_div" style="display:none;font-size:16px">
        <div>Codice promo <span id='valid_code_span' class="bold"></span></div>
        <br>
        <div class="left box-desc"><span id='valid_desc_span' class="bold"></span></div>
        <div class="right">
            <input type="button" id="continue_btn" class="effect7" value="Continua" onclick="document.location.href = 'http://www.google.it'" />
        </div>
        <div class="clear"></div>
    </div>
    <div id="requests_response_div" style="display:none;font-size:16px">
        <div>Codice promo <span id='requests_code_span' class="bold"></span></div>
        <br>
        <div class="left box-desc"><span id='requests_desc_span' class="bold"></span></div>
        <div class="right">
            <input type="button" id="continue_btn" class="effect7" value="Richieste in attesa" onclick="document.location.href = 'http://www.google.it'" />
        </div>
        <div class="clear"></div>
    </div>
    <div id="confirmed_response_div" style="display:none;font-size:16px">
        <div class="left">
            <div>Codice promo <span id='confirmed_code_span' class="bold"></span></div>
            <br>
            <div class="box-desc"><span id='confirmed_desc_span' class="bold"></span></div>
        </div>
        <div class="right bold">
            Prenotazione confermata<br>
            Nome struttura <span id='str_value'></span><br>
            Localit&agrave; <span id='loc_value'></span><br>
            Data Prenotazione: <span id='data_value'></span>
        </div>
        <div class="clear"></div>
    </div>
    <div id="expired_response_div" style="display:none;font-size:16px">
        <div>Codice promo <span id='expired_code_span' class="bold"></span></div>
        <br>
        <div class="left box-desc"><span id='expired_desc_span' class="bold"></span></div>
        <div class="right">
            <input type="button" id="continue_btn" class="effect7" value="Sostitusci" onclick="document.location.href = 'http://www.google.it'" />
        </div>
        <div class="clear"></div>
    </div>
</div>




<div id='attending' style="display:none;font-size:24px;color:#FF0000;border-color:#000000"  align="center">Attendi...</div> 
<br /><br />


<script language="javascript">
                $("#verify").click(function() {
                    var textval = $("#code_text").val();
                    if (textval == '')
                    {
                        $("#confirmed_response_div").hide();
                        $("#valid_response_div").hide();
                        $("#not_valid_response_div").hide();
                        $("#requests_response_div").hide();
                        $("#expired_response_div").hide();
                        $("#attending").hide();
                        $("#verify_div").show();
                        $("#error_msg").show();
                    }
                    else
                    {
                        $("#error_msg").hide();
                        $("#verify_div").hide();
                        $("#confirmed_response_div").hide();
                        $("#vaild_response_div").hide();
                        $("#not_valid_response_div").hide();
                        $("#requests_response_div").hide();
                        $("#expired_response_div").hide();
                        $("#verify_div").hide();

//$("#attending").show();
                        $("#attending").hide();
                        $.ajax({url: "<?php echo JURI::base(); ?>api_data.php?code=" + textval, success: function(data)
                            {
                                data = $.parseJSON(data);
                                var result = data.status
                                //alert(result);
                                //	var result = data[0].status; 
                                $("#attending").hide();
                                if (result == 'not_valid')
                                {
                                    $("#confirmed_response_div").hide();
                                    $("#requests_response_div").hide();
                                    $("#valid_response_div").hide();
                                    $("#expired_response_div").hide();
                                    $("#not_valid_response_div").show();

                                }
                                else if (result == 'requests')
                                {
                                    $("#code_wrapper").hide();
                                    $("#confirmed_response_div").hide();
                                    $("#not_valid_response_div").hide();
                                    $("#valid_response_div").hide();
                                    $("#expired_response_div").hide();
                                    $("#requests_code_span").html(data.code);
                                    $("#requests_desc_span").html(data.box);
                                    $("#requests_response_div").show();
                                }
                                else if (result == 'valid')
                                {
                                    $("#code_wrapper").hide();
                                    $("#confirmed_response_div").hide();
                                    $("#not_valid_response_div").hide();
                                    $("#requests_response_div").hide();
                                    $("#expired_response_div").hide();
                                    $("#valid_response_div").show();
                                    $("#valid_code_span").html(data.code);
                                    $("#valid_desc_span").html(data.box);
                                }
                                else if (result == 'confirmed')
                                {
                                    $("#code_wrapper").hide();
                                    $("#not_valid_response_div").hide();
                                    $("#requests_response_div").hide();
                                    $("#valid_response_div").hide();
                                    $("#expired_response_div").hide();
                                    $("#box_value").html(data.box);
                                    $("#str_value").html(data.accomodation);
                                    $("#data_value").html(data.date);
                                    /*$("#dal_value").html(data.from);
                                     $("#al_value").html(data.to);*/
                                    $("#loc_value").html(data.location);
                                    $("#confirmed_code_span").html(data.code);
                                    $("#confirmed_desc_span").html(data.box);
                                    $("#confirmed_response_div").show();
                                }
                                else if (result == 'expired')
                                {
                                    $("#code_wrapper").hide();
                                    $("#confirmed_response_div").hide();
                                    $("#not_valid_response_div").hide();
                                    $("#valid_response_div").hide();
                                    $("#requests_response_div").hide();
                                    $("#expired_code_span").html(data.code);
                                    $("#expired_desc_span").html(data.box);
                                    $("#expired_response_div").show();
                                }
                                
                                $("#code_text").val("");
                                $("#verify_div").show();
                            }
                        });
                    }
                });

</script>