<?php
defined('_JEXEC') or die();
jimport( 'joomla.application.component.modellist' );
require JPATH_BASE . "/moveboxconfig.php";

class MoveboxModelAccomodation extends JModelList
{
    function getAccomodation()
    {
        
        try {
            $boxId = $_GET['box_id'];
            $accomodationId =  $_GET['acc_id'];

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => ACCOMODATION_REST_URL."/".$boxId."/".$accomodationId
            ));
            $resp = curl_exec($curl);          
            curl_close($curl);
            
            //$resp = $resp . "}";
            
            $respObj = json_decode(trim ($resp));
            return $respObj;
        } catch (Exception $e) {
            error_log("getAccomodation - ERROR");
            return $e->getMessage();
        }
    }
}