<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
$document->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js');
$document->addStyleSheet('components/com_movebox/assets/css/movebox.css');
$document->addStyleSheet('components/com_movebox/assets/css/jquery.bxslider.css');
$document->addScript('components/com_movebox/assets/js/jquery.bxslider.min.js');
$document->addScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyBOKmRsszh5Z6SVFU5mZEiq_F3kXQIH1sI&sensor=false');
$document->addScriptDeclaration("
//<![CDATA[
    function initialize() {
        var myLatlng = new google.maps.LatLng(" . $this->lat . ", " . $this->lon . ");
        var mapOptions = {
            center: myLatlng,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
        
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: '" . $this->name . "'
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    
    jQuery(document).ready(function($) {
        $('.bxslider').bxSlider({
            adaptiveHeight: false,
            pager: false
        });
    });
    
//]]>
");


?>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:italic" rel="stylesheet" type="text/css">
<div id="movebox-title">
    <div class="float-left">
        <div id="title-name"><?= $this->name ?></div>
        <div id="title-address"><?= $this->fullAddress ?></div>
    </div>
    <div class="float-right"><input type="button" onclick="history.go(-1);" value="Torna alla lista"/></div>
</div>
<div id="images-slider" class="float-left">
    <ul class="bxslider">
        <?php foreach ($this->photoArray as $image) { ?>

            <li><div id="slide" style="background-image:url('<?= str_replace("localhost:8080", "95.211.231.36/booking", $image) ?>')"></div></li>

        <?php } ?>               
    </ul>
</div>
<div id="movepass" class="float-right">
    <?= $this->movepass ?>
</div>
<div id="description" class="float-clear"><?= $this->desc ?></div>
<div id="website">website: <a href="<?= $this->webSiteUrl ?>" target="_blank" class="no-link"><?= $this->webSite ?></a></div>

<hr>
<div id="movebox-middle">
    <div id="facilities">
        <div class="section-title">Servizi</div>
        <ul>
            <?php foreach ($this->facilitiesArray as $facility) { ?>
                <li><?= $facility ?></li>
            <?php } ?>               
        </ul>
    </div>
    <div id="map">
        <div class="section-title">Mappa</div>
        <div id="google-map"></div>
    </div>
    <div id="address-contacts">
        <div class="section-title">Indirizzo</div>
        <div id="address-name"><?= $this->name ?></div>
        <div id="address-street">
            <?= $this->address ?>
            <?= $this->addressNmber ?>
        </div>
        <div id="address-cap">
            CAP: <?= $this->cap ?>
        </div>
        <div id="address-city">
            <?= $this->comune ?>
        </div>
        <div id="address-province">
            <?= $this->provincia ?>
        </div>
        <div id="contacts">
            <div id="contacts-phone">
                Tel: <?= $this->phone ?>
            </div>
            <div id="contacts-email">
                Email: <a href="mailto:<?= $this->email ?>" class="no-link"><?= $this->email ?></a>
            </div>
        </div>
    </div>
</div>
