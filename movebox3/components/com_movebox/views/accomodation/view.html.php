<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view' );
class MoveboxViewAccomodation extends JView
{
    function display($tpl = null)
    {   
        $userLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        
        $accomodation = $this->get('Accomodation');
       
        // Name - Start
	$this->name = "";
	$namesArray = $accomodation->name;

	if(count($namesArray) == 1) {
		$this->name = $accomodation->name[0]->text;
	} else {
            foreach ($namesArray as $nameElement) {
                if ($nameElement->lang == $userLang) {
                    $this->name = $nameElement->text;
                    break;
                } else if ($nameElement->lang == "en") {
                    $this->name = $nameElement->text;
                }
            }
            if ($this->name == "") {
                $this->name = $namesArray[0]->text;
            }
	}
	// Name - End	
	
	// Description - Start
	$this->desc = "";
	$descsArray = $accomodation->description;

	if(count($descsArray) == 1) {
		$this->desc = $accomodation->description[0]->text;
	} else {
            foreach ($descsArray as $descElement) {
                if($descElement->lang == $userLang) {
                        $this->desc = $descElement->text;
                        break;
                } else if ($descElement->lang == "en") {
                        $this->desc = $descElement->text;
                }
            }
            if($this->desc == "") {
                $this->desc = $descsArray[0]->text;
            }
	}
	// Description - End
	
        // Address - Start
        $this->address = $accomodation->address;
        // Address - End
                
         // Address number - Start
        $this->addressNmber = $accomodation->addressNumber;
        // Address number - End
        
        // Cap - Start
        $this->cap = $accomodation->comune->code;
        // Cap - End
        
        // Comune - Start
        $this->comune = $accomodation->comune->name;
        // Comune - End
        
        // Provincia - Start
        $this->provincia = $accomodation->province->name;
        // Provincia - End
                
        $this->fullAddress =$this->address . " " . $this->addressNmber . ", ". $this->cap . " " . $this->comune . " (" . $this->provincia . ")";
        
        // LAT - Start
        $this->lat = $accomodation->lat;
           
        if($this->lat == "")
            $this->lat = 0;
        // LAT - End
        
        // LON - Start
        $this->lon = $accomodation->lng;
        
	if($this->lon == "")
            $this->lon = 0;
        // LON - End
        
        // EMAIL - Start
        $this->email = $accomodation->email;
        // EMAIL - End
        
        // PHONE - Start
        $this->phone = $accomodation->phone;
        // PHONE - End
        
        // WEBSITE - Start
        $this->webSite = $accomodation->webSite;
        
        if (strpos($this->webSite, 'http') === 0) {
            $this->webSiteUrl = $this->webSite;
        } else {
            $this->webSiteUrl = "http://" . $this->webSite;
        }
        
        // WEBSITE - End
        
	// Photos - Start
	$this->photoArray = $accomodation->photos;
               
	/*if(photoUrl == "") {
            $photoArray = "no image";
	}*/ 
        // Photos - End	
        
        
        // Movepass - Start
        $this->movepass = "";
	$movepassesArray = $accomodation->movepass;

	if(count($movepassesArray) == 1) {
		$this->movepass = $accomodation->movepass[0]->text;
	} else {
            foreach ($movepassesArray as $movepassElement) {
                if($movepassElement->lang == $userLang) {
                        $this->movepass = $movepassElement->text;
                        break;
                } else if ($movepassElement->lang == "en") {
                        $this->movepass = $movepassElement->text;
                }
            }
            if($this->movepass == "") {
                $this->movepass = $movepassesArray[0]->text;
            }
	}
        
        $this->movepass = nl2br($this->movepass);
        // Movepass - End
        
        // Facilities - Start
        $this->facilities = "";
        $this->facilitiesArray = array();
	$mainFacilitiesArray = $accomodation->facilities;

         foreach ($mainFacilitiesArray as $mainFacilityElement) {
            $namesArray = $mainFacilityElement->name;
            error_log($namesArray[0]->text);
            $this->facilities = "";
           
            if(count($namesArray) == 1) {
                    $this->facilities = $namesArray[0]->text;
            } else {
                foreach ($namesArray as $facilityElement) {
                    if($facilityElement->lang == $userLang) {
                            $this->facilities = $facilityElement->text;
                            break;
                    } else if ($facilityElement->lang == "en") {
                            $this->facilities = $facilityElement->text;
                    }
                }
                if($this->facilities == "") {
                    $this->facilities = $namesArray[0]->text;
                }
            }
            error_log($this->facilities);
            array_push ($this->facilitiesArray, $this->facilities);
         
         }
        // Facilities - End
        
        parent::display($tpl);
    }
}