<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
$document->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js');
$document->addStyleSheet('components/com_movebox/assets/css/movebox.css');
?>

<h1>Verifica il tuo codice</h1>

<form action="<?php echo JRoute::_("index.php?option=com_movebox&task=checkcode"); ?>" method="post" name="checkForm" id="checkForm">
    <input id="movebox_code" type="text" /> <input type="submit" value="Verifica" />
</form>

<?= $this->code ?>