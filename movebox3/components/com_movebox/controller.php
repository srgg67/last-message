<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla controller library
jimport('joomla.application.component.controller');
/**
 * General Controller of Anagrafiche component
 */
class MoveboxController extends JController
{
    public function display($cachable = false, $urlparams = false)
    {
       // set default view if not set
        JRequest::setVar('view', JRequest::getCmd('view', 'accomodation'));
        
        // call parent behavior
        parent::display($cachable);
    }
    
    public function checkcode($cachable = false, $urlparams = false)
    {
        JRequest::setVar('view', JRequest::getCmd('view', 'checkcode'));
        parent::display($cachable);
    }
}