<?php
/* ------------------------------------------------------------------------
  # mod_klixo_articles_slideshow  - Version 1.2.0
  # ------------------------------------------------------------------------
  # Copyright (C) 2011 - 2012 Klixo. All Rights Reserved.
  # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Author: JF Thier Klixo
  # Website: http://www.klixo.se
  ------------------------------------------------------------------------- */

defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__) . DS . 'helper.php');
require_once (dirname(__FILE__) . DS . 'libs' . DS . 'articles_slideshow.php');

jimport("joomla.filter.filterinput");

$resizeImage = $params->get("resizeimage", "1");

if ($resizeImage == 1) {
    jimport("joomla.filesystem.folder");
    jimport("joomla.filesystem.file");
}

if (!$poswidth = strpos($params->get('thumb_width', "120px"), "px")) {
    $poswidth = $params->get('thumb_width', "120px") . "px";
}
if (!$posheight = strpos($params->get('thumb_height', "150px"), "px")) {
    $posheight = $params->get('thumb_height', "150px") . "px";
}

$target =                       $params->get("target", '');
$jquery =                       $params->get("jquery", 0);
$pause =                        $params->get("pause", 'true');
$transition =                   $params->get("transition", 'fade');
$randomizeEffects =             $params->get("randomizeEffects", 1);
$auto_play =                    $params->get("auto_play", '1');
$slideshow_speed =              $params->get("slideshow_speed", 800) * 1000;
$timer_speed =                  $params->get("timer_speed", 4000) * 1000;
$navBarColor =                  $params->get("navBarColor", '#ccc');
$title_color =                  $params->get("title_color", '#FFFFFF');
$title_font_size =              $params->get("title_font_size", '16');
$prenext_show =                 $params->get("prenext_show", 1);
$show_title =                   $params->get("show_title", 'true');
$thumb_height =                 $params->get('thumb_height', "940");
$thumb_width =                  $params->get('thumb_width', "450");
$show_readmore =                $params->get('show_readmore', "0");
$ReadMore_font_size =           $params->get('ReadMore_font_size', "14");
$show_description =             $params->get('show_description', "0");
$description_color =            $params->get('description_color', "#FFFFFF");
$content_font_size =            $params->get('content_font_size', "14");
$link_title =                   $params->get('link_title', 1);
$link_image =                   $params->get('link_image', 1);
$img_Pos =                      $params->get('img_Pos', 'left');
$button_style =                 $params->get('button_style', 'number');
$desc_box_width =               $params->get('slideShow_width') - $thumb_width;
$slideShow_width =              $params->get('slideShow_width', '600');
$slideShow_background =         $params->get('slideShow_background', '#000000');
$read_more_color =              $params->get('read_more_color', '#000000');
$readmore_Btn =                 JText::_('MORE_INFO');

 $transitionList="";
foreach ($transition as $key) {
  $transitionList=="" ?  $transitionList.=  $key :  $transitionList.= ','. $key;
}

$center = round($thumb_height / 2);
$bottom = 220;
$widthIe = 0;
if ($center > $bottom)
    $bottom = $center;

jimport('joomla.environment.browser');
 $browser = &JBrowser::getInstance();
 $browserName =$browser->getBrowser();
 $browserVersion =$browser->getMajor();


if ($browserName == 'msie' && $browserVersion == 6) {
    $widthIe = 3;
}
if (!defined('ARTICLES_SLIDESHOW')) {
    define('ARTICLES_SLIDESHOW', 1);

    if ($jquery) {
        JHTML::script('jquery-1.5.1.min.js', JURI::base() . '/modules/' . $module->module . '/assets/');
    }

    /* Add css */
    JHTML::stylesheet('style.css', JURI::base() . '/modules/' . $module->module . '/assets/');

    if ($browserName== 'msie' && $browserVersion == 6) {
        JHTML::stylesheet('ie6.css', JURI::base() . '/modules/' . $module->module . '/assets/');
        $crapIE6 = true;
        /*if ($jquery) {
        JHTML::script('jquery.pngFix.js', JURI::base() . '/modules/' . $module->module . '/assets/');
    }*/
        
    } else if ($browserName == 'msie' && $browserVersion == 7) {
        JHTML::stylesheet('ie7.css', JURI::base() . '/modules/' . $module->module . '/assets/');
        $currentBrowser = IE7;
    }

    JHTML::script('jquery.cycle.all.js', JURI::base() . '/modules/' . $module->module . '/assets/');
}

$GLOBALS["module"] = $module;
$slideShow = new articles_slideshow();

$slideShow->id = $module->id;
$slideShow->enable = 1;
$slideShow->web_url = JURI::base();
$slideShow->thumb_height = $posheight ? $posheight : $params->get('thumb_height', "150px");
$slideShow->thumb_width = $poswidth ? $poswidth : $params->get('thumb_width', "120px");
$slideShow->cropresizeimage = $params->get('cropresizeimage', 0);
$slideShow->reformat_content = $params->get('reformat_content', 1);
$slideShow->max_description = $params->get('limit_description', 200);
$slideShow->max_title = $params->get('limittitle', 25);
$slideShow->resize_folder = JPATH_CACHE . DS . $module->module . DS . "images";
$slideShow->url_to_resize = JURI::base() . "cache/" . $module->module . "/images/";
$slideShow->url_to_module = JURI::base(). "modules/" . $module->module . "/libs/";
$slideShow->imgPadding = $params->get('imgPadding', 0);
$slideShow->backgroundStyle = $params->get('bgd_Style', 'solid');
$slideShow->bgdStartColor = $params->get('bgdStartColor', '#000');
$slideShow->bgdEndColor = $params->get('bgdEndColor', '#000');


$slideShow->items = modArticlesSlideshowHelper::getContentList($params);
$slideShow->build();
$items = $slideShow->getItems();

$themePath = JModuleHelper::getLayoutPath('mod_articles_slideshow');
if (file_exists($themePath)) {
    require($themePath);
}
?>