<?php

/* ------------------------------------------------------------------------
  # mod_klixo_articles_slideshow  - Version 1.2.0
  # ------------------------------------------------------------------------
  # Copyright (C) 2011 - 2012 Klixo. All Rights Reserved.
  # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Author: JF Thier Klixo
  # Website: http://www.klixo.se
  ------------------------------------------------------------------------- */

// no direct access
defined('_JEXEC') or die('Restricted access');


if (!class_exists("articles_slideshow")) {

    jimport('joomla.filesystem.file');
    include_once("includes/simpleimage.php");

    class articles_slideshow {

        public $id = '';
        public $items = array();
        public $thumb_width = '150';
        public $thumb_height = '100';
        public $web_url = '';
        public $cropresizeimage = 0;
        public $imgPadding = 0;
        public $backgroundStyle = 'solid';
        public $bgdStartColor = '#000';
        public $bgdEndColor = '#000';
        public $max_title = 0;
        public $max_description = 0;
        public $resize_folder = '';
        public $url_to_resize = '';
        public $url_to_module = '';
        private $noItemLink = false;
        private $prefs = "";
        private $changedPrefs = false;

        /* function __construct() {

          } */

        public function build() {

            if (!JFolder::exists($this->resize_folder)) {
                JFolder::create($this->resize_folder);
            }

            $prefs .= $this->thumb_width . '-';
            $prefs .= $this->thumb_height . '-';
            $prefs .= $this->cropresizeimage . '-';
            $prefs .= $this->imgPadding . '-';
            $prefs .= $this->backgroundStyle . '-';
            $prefs .= $this->bgdStartColor . '-';
            $prefs .= $this->bgdEndColor;

            $this->prefs = $prefs;

            $this->getPrefs();

            $this->process();
        }

        public function getItems() {
            return $this->items;
        }

        public function getWidth($thumbsInRow = 6, $thumbPadding = 5, $thumbSpacing = 10) {

            ($thumbsInRow % 2 >= 1) ? $indent = 0 : $indent = 1;

            return($thumbsInRow * ($this->thumb_width + $thumbPadding * 2) ) + ( $thumbSpacing * 2 * $thumbsInRow);
        }

        public function getHeight($limit = 6, $height = 125, $thumbsInRow = 6, $thumbPadding = 5, $thumbSpacing = 10) {

            $rows = $thumbsInRow > 0 ? ceil($limit / $thumbsInRow) : 1;

            return ($rows * ($height + $thumbPadding * 2) ) + ( $thumbSpacing * 2 * ($rows));
        }

        private function getPrefs() {

            $prefPath = $this->resize_folder . "_prefs.txt";

            if (!JFile::exists($prefPath)) {
                JFile::write($prefPath, $this->prefs);
            } else {
                $savedPrefs = JFile::read($prefPath);
                if ($savedPrefs != $this->prefs) {
                    JFile::write($prefPath, $this->prefs);
                    $this->changedPrefs = true;
                }
            }
        }

        private function process() {
            $items = array();

            /* Check elements */
            if (sizeof($this->items) < $this->element_number) {
                $this->element_number = sizeof($this->items);
            }
            foreach ($this->items as $key => $item) {
                if (!isset($item->sub_title)) {
                    $item->sub_title = $this->cutStr($item->title, $this->max_title);
                }

                if (!isset($item->sub_content)) {

                    if ($this->reformat_content == 1) {
                        $item->sub_content = $this->cutStr($item->content, $this->max_description);
                    } else {
                        $item->sub_content = $item->content;
                    }
                }

                if (!isset($item->thumb)) {
                    $item->thumb = $this->processImage($item->img, $this->thumb_width, $this->thumb_height);
                }

                $this->items[$key] = $item;
                if ($this->noItemLink == true) {
                    $item->img = $this->url_to_module . "html/no_content_err.html";
                    $this->noItemLink = false;
                }
                $this->items[$key] = $item;
            }
        }

        function processImage($imagePath, $width, $height) {
            global $module;
            $rebuildThumbnail = false;
            $cropOrResize = $this->cropresizeimage;
            $thumbnailsPath = $this->resize_folder;

            $file_CompleteName = str_replace('/', '', strrchr($imagePath, "/"));
            $file_name = JFile::stripExt($file_CompleteName);
            $file_ext = NULL;
            $name_withPath =  JFile::stripExt($imagePath);
            //if (is_file($imagePath))
            if (@fopen($imagePath, "r")) {
                $file_ext = JFile::getExt($imagePath);
                $thumb_ext = $this->backgroundStyle == "transparent" ? 'png' : $file_ext;
                $thumb_name = "thumb_" . JFile::makeSafe($name_withPath) . '.' . $thumb_ext;
                $ext = strtoupper($file_ext);
                //$ext = $file_ext ? strtoupper($file_ext): 'PNG';		
            }

            $validFormats = array('GIF', 'JPG', 'PNG', 'JPEG', 'SWF');

            if (!$file_ext || !in_array($ext, $validFormats)) {
                $ext = 'PNG';
                if (!preg_match("/http/i", $imagePath)) { // Test if the image path is a link extracted from the object code. ie. Youtube code snippet
                    $imagePath = $this->url_to_module . "imgs/no_content.png";
                    $this->noItemLink = true;
                    $thumb_name = "thumb_nocontent.png";
                    $cropOrResize = 0;
                } else {
                    $imagePath = $width >= 64 || $height >= 64 ? $this->url_to_module . "imgs/html_link64.png" : $this->url_to_module . "imgs/html_link32.png";
                    //$imagePath = $this->url_to_module . "imgs/html_link.png";
                    $thumb_name = "thumb_html.png";
                    $cropOrResize = 1;
                }
            }

            if ($ext == "SWF") {

                $imagePath = $width >= 64 || $height >= 64 ? $this->url_to_module . "imgs/flash_swf_64.png" : $this->url_to_module . "imgs/flash_swf_32.png";
                //$imagePath = $this->url_to_module . "imgs/flash_swf.png";
                $thumb_name = "thumb_swf.png";
                $ext = 'PNG';
                $cropOrResize = 1;

                /* $embedFlash = '
                  <object height="' . $height . '" width="' . $width . '" 		codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
                  <param name="src" value="' . $imagePath . '" />
                  <param name="quality" value="autohigh" />
                  <param name="wmode" value="transparent" />
                  <param name="base" value="/" />
                  <param name="name" value="' . $file_CompleteName . '" /><embed height="' . $height . '" width="' . $width . '" name="' . $file_CompleteName . '" base="/" wmode="window" quality="autohigh" src="' . $imagePath . '" type="application/x-shockwave-flash"></embed>
                  </object>';

                  return  $embedFlash; */
            }

            if (!JFile::exists($thumbnailsPath . DS . $thumb_name)) { // No thumbnail exists in the cache for this image
                $rebuildThumbnail = true;
            }

            if ($rebuildThumbnail == true || $this->changedPrefs == true) {
                $image = new SimpleImage();
                $image->load($imagePath);
                $image->makeThumb($width, $height, $this->backgroundStyle, $this->bgdStartColor, $this->bgdEndColor, $cropOrResize, $this->imgPadding);
                $saveAlpha = $this->backgroundStyle == "transparent" ? TRUE : FALSE;
                $image->save($thumbnailsPath . DS . $thumb_name, $ext, $saveAlpha);
                unset($image);
            }
            return $this->url_to_resize . $thumb_name;
        }

        /* Cut string */

        function cutStr($str, $maxLenght) {

            $str = strip_tags($str);

            if (strlen($str) <= $maxLenght) {
                return $str;
            }
            return substr($str, 0, $maxLenght) . ' [...]';
        }

    }

}
?>