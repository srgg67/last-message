<?php
/* ------------------------------------------------------------------------
  # mod_klixo_articles_slideshow  - Version 1.2.0
  # ------------------------------------------------------------------------
  # Copyright (C) 2011 - 2012 Klixo. All Rights Reserved.
  # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Author: JF Thier Klixo
  # Website: http://www.klixo.se
  ------------------------------------------------------------------------- */

defined('_JEXEC') or die('Restricted access');



if(!class_exists("SimpleImage")){



class SimpleImage {

   public $image; 
   public $image_type;
   public $bgd_style;


   function load($filename) 
   {
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
	  
      if( $this->image_type == IMAGETYPE_JPEG )
	  {
         $this->image = imagecreatefromjpeg($filename);
      } 
	  elseif( $this->image_type == IMAGETYPE_GIF )
	  {
        $this->image = imagecreatefromgif($filename);

      } 
	  elseif( $this->image_type == IMAGETYPE_PNG )
	  {
        $this->image = imagecreatefrompng($filename);

      }
   }



   function save($filename, $ext="PNG", $saveAlpha = NULL ,$compression=75, $permissions=NULL  )
   {
	  
	  if ($saveAlpha == 1 || $ext == 'PNG') {
		   imagepng($this->image,$filename);
	  }
	  else 
	  {	  
		if( $ext == 'JPG' || $ext == 'JPEG' )
		{
		   imagejpeg($this->image,$filename,$compression);
		}
		elseif( $ext == 'GIF' )
		{
		   imagegif($this->image,$filename);         
		}
	  
	  }
	   
	  
      if( $permissions != null)
	  {
         chmod($filename,$permissions);
      }
	  
	  imagedestroy ($this->image);
   }


   function getWidth()
   {
      return imagesx($this->image);
   }

   function getHeight()
   {
      return imagesy($this->image);
   }


  function makeThumb($width,$height, $backgroundStyle, $bgdStartColor, $bgdEndColor, $crop,  $padding) {
  
	  if ($crop == false) {
		  //Resize mode: we need to resize the image before creating the thumbnail
			$targetH  = $height - $padding <24  ? 24 : $height - $padding; // To avoid inconsistent results with thumbnail height
			$targetW  = $width - $padding <24  ? 24 : $width - $padding;  // o avoid inconsistent results with thumbnail width
			$this->image =  $this->scaleImage($targetW,$targetH);  
	  }
 
	  $src_Width = $this->getWidth();
	  $src_Height = $this->getHeight();
	  $startX=  $this->getStartX($src_Width, $width);
	  $startY=  $this->getStartY($src_Height, $height);
  
	  $dest_Img = imagecreatetruecolor($width, $height);
	  imagesavealpha($dest_Img, true);
	
	if  ($backgroundStyle == 'solid') {
		$bgdColor = $this->rgb2array($bgdStartColor);
		$solidColor = imagecolorallocate($dest_Img,$bgdColor[0],$bgdColor[1],$bgdColor[2]);
		imagefill($dest_Img, 0, 0, $solidColor);
	} 
	else if  ($backgroundStyle == 'transparent') {
   		 imagefill($dest_Img, 0, 0, imagecolorallocatealpha($dest_Img, 0, 0, 0, 127));
	} 
	
	else {
	  require_once('gradient_fill.php');
	  $background = new gd_gradient_fill($width,$height,$backgroundStyle,$bgdStartColor,$bgdEndColor,0);
	  imagecopy ($dest_Img ,$background->image , 0, 0 , 0 , 0 ,$width , $height );
	  imagedestroy($background->image);
	}
  
	 imagecopyresampled ( $dest_Img ,  $this->image ,  $startX  ,  $startY ,0 , 0 ,  $src_Width ,   $src_Height , $src_Width , $src_Height  );
	  $this->image = $dest_Img;   
  
	 } 
	 

   function scaleImage($targetW,$targetH)
   {
	   
	  $src_Width = $this->getWidth();
	  $src_Height = $this->getHeight();
	  
	  $scalingH = $targetH / $src_Height;
	  $scalingW = $targetW / $src_Width;
	  $scaling =  min($scalingH, $scalingW);
      $newWidth = round($this->getWidth() * $scaling) ;
	  $newHeight = round($this->getHeight() * $scaling) ;
	  
	  //Create new source image resized
      $scaledImg = imagecreatetruecolor( $newWidth, $newHeight);
	  imagealphablending($scaledImg, false);
	  imagesavealpha($scaledImg, true);	  
	  imagecopyresampled ( $scaledImg , $this->image , 0 ,0 , 0 ,0 ,$newWidth ,  $newHeight ,  $src_Width , $src_Height) ;
	  return  $scaledImg;
   }


	   

  function getStartX($SrcWidth, $DestWidth)
    {
     $x1 =( $DestWidth -$SrcWidth )/2; 
	 return $x1;
	}
  
  
  
  function getStartY($SrcHeight , $DestHeight)
   {
     $y1 = ($DestHeight - $SrcHeight)/2;
	 return $y1;
   }
  
  /*
   * From php.net
   * Convert color from hex in XXXXXX (eg. FFFFFF, 000000, FF0000) to array(R, G, B)
   * of integers (0-255).
   *
   * name: rgb2array
   * author: Yetty
   * @param $color hex in XXXXXX (eg. FFFFFF, 000000, FF0000)
   * @return string; array(R, G, B) of integers (0-255)
   */
  function rgb2array($rgb) {
	  if (substr($rgb,0, 1) == '#' ) {
	  $rgbC =	substr($rgb,1);
	  } else {
		   $rgbC =$rgb;
	  }
	  return array(
		  base_convert(substr($rgbC, 0, 2), 16, 10),
		  base_convert(substr($rgbC, 2, 2), 16, 10),
		  base_convert(substr($rgbC, 4, 2), 16, 10),
	  );
  }  


  }

}
?>