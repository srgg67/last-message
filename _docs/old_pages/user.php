﻿<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
        <div class="art-content-layout">
            <div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                	<article class="art-post">
      					<div class="art-postcontent clearfix">
    						<fieldset id="user_data">
    							<h2>Add/Edit billing address information</h2>
<?	//checkoutForm	?>    
								<div id="control-buttons" class="control-buttons">
    								<div>
    Please use <strong>Register And Checkout</strong> to easily get access to your order history, or use <strong>Checkout as Guest</strong>
    								</div>
                                    <div>
                                        <button class="psbutton vm-button-correct art-button" type="submit" onclick="javascript:return callValidatorForRegister(userForm);" title="Register And Checkout">Register And Checkout</button>
                                        
                                        <button class="psbutton vm-button-correct art-button" title="Checkout as Guest" type="submit" onclick="javascript:return myValidator(userForm, 'savecartuser');">Checkout as Guest</button>
                                    </div>
<? 	//<button class="default" type="submit" onclick="javascript:return callValidatorForRegister(userForm);" title="Register And Checkout">Register And Checkout</button>
	//<button class="default" title="Checkout as Guest" type="submit" onclick="javascript:return myValidator(userForm, &#39;savecheckoutuser&#39;);">Checkout as Guest</button>
    //<button class="default" type="reset" onclick="window.location.href=&#39;/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=cart&amp;lang=en&#39;">Cancel</button>?>
								</div>
        						<fieldset>
            						<span class="userfields_info">Shopper Information</span>
        						</fieldset>
                                <table class="adminForm user-details">
                                
                                            <tbody><tr>
                                        <td class="key" title="">
                                            <label class="email" for="email_field">
                                                E-Mail *						</label>
                                        </td>
                                        <td>
                                            <input type="text" id="email_field" name="email" size="30" value="" class="required" maxlength="100" aria-required="true" required> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="username" for="username_field">
                                                Username						</label>
                                        </td>
                                        <td>
                                            <input type="text" id="username_field" name="username" size="30" value="" maxlength="25"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="name" for="name_field">
                                                Displayed Name						</label>
                                        </td>
                                        <td>
                                            <input type="text" id="name_field" name="name" size="30" value="" maxlength="25"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="password" for="password_field">
                                                Password						</label>
                                        </td>
                                        <td>
                                            <input type="password" id="password_field" name="password" size="30" class="inputbox">
                                        </td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="password2" for="password2_field">
                                                Confirm Password						</label>
                                        </td>
                                        <td>
                                            <input type="password" id="password2_field" name="password2" size="30" class="inputbox">
                                        </td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="agreed" for="agreed_field">
                                                I agree to the Terms of Service						</label>
                                        </td>
                                        <td>
                                            <input type="checkbox" name="agreed" id="agreed_field" value="1">					</td>
                                    </tr>
                                
                                </tbody></table>
    						                            
    							<span class="userfields_info">Bill To</span>
                                <table class="adminForm user-details">
                            
                                        <tbody><tr>
                                    <td class="key" title="">
                                        <label class="company" for="company_field">
                                            Company Name						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="company_field" name="company" size="30" value="" maxlength="64"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="title" for="title_field">
                                            Title						</label>
                                    </td>
                                    <td>
                                        <select id="title" name="title">
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            </select>
                                    </td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="first_name" for="first_name_field">
                                            First Name *						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="first_name_field" name="first_name" size="30" value="" class="required" maxlength="32"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="middle_name" for="middle_name_field">
                                            Middle Name						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="middle_name_field" name="middle_name" size="30" value="" maxlength="32"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="last_name" for="last_name_field">
                                            Last Name *						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="last_name_field" name="last_name" size="30" value="" class="required" maxlength="32"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="address_1" for="address_1_field">
                                            Address 1 *						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="address_1_field" name="address_1" size="30" value="" class="required" maxlength="64"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="address_2" for="address_2_field">
                                            Address 2						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="address_2_field" name="address_2" size="30" value="" maxlength="64"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="zip" for="zip_field">
                                            Zip / Postal Code *						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="zip_field" name="zip" size="30" value="" class="required" maxlength="32"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="city" for="city_field">
                                            City *						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="city_field" name="city" size="30" value="" class="required" maxlength="32"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="virtuemart_country_id" for="virtuemart_country_id_field">
                                            Country *						</label>
                                    </td>
                                    <td>
                                        <select id="virtuemart_country_id" name="virtuemart_country_id" class="virtuemart_country_id required">
                            <option value="" selected="selected">-- Select --</option>
                            <option value="105">Italy</option>
                            </select>
                                    </td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="virtuemart_state_id" for="virtuemart_state_id_field">
                                            State / Province / Region *						</label>
                                    </td>
                                    <td>
                                        <select class="inputbox multiple" id="virtuemart_state_id" size="1" name="virtuemart_state_id" required="">
                                        <option value="">-- Select --</option>
                                        </select>					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="phone_1" for="phone_1_field">
                                            Phone						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="phone_1_field" name="phone_1" size="30" value="" maxlength="32"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="phone_2" for="phone_2_field">
                                            Mobile Phone						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="phone_2_field" name="phone_2" size="30" value="" maxlength="32"> 					</td>
                                </tr>
                                    <tr>
                                    <td class="key" title="">
                                        <label class="fax" for="fax_field">
                                            Fax						</label>
                                    </td>
                                    <td>
                                        <input type="text" id="fax_field" name="fax" size="30" value="" maxlength="32"> 					</td>
                            
                                        </tbody>
                            </table>
                            </fieldset>

                            <fieldset id="shipping_data">
								<h2>Add/Edit shipment address</h2>
	<?	/*<form method="post" id="userForm" name="userForm" class="form-validate">
		<!--<form method="post" id="userForm" name="userForm" action="/movebox3/en/component/virtuemart/" class="form-validate">-->*/?>
									<?	/*
                                    <div class="control-buttons">			
                                        <button class="default" type="submit" onclick="javascript:return myValidator(userForm, 'savecartuser');">Save</button>
                                        <button class="default" type="reset" onclick="window.location.href='/movebox3/en/component/virtuemart/cart?Itemid=0'">Cancel</button>
									</div>*/	?>
                                <table class="adminForm user-details">
                    
                                            <tbody><tr>
                                        <td class="key" title="">
                                            <label class="shipto_address_type_name" for="shipto_address_type_name_field">
                                                Address Nickname *						</label>
                                        </td>
                                        <td>
                                            <input required="required" aria-required="true" id="shipto_address_type_name_field" name="shipto_address_type_name" size="30" value="" class="required" maxlength="32" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_company" for="shipto_company_field">
                                                Company Name						</label>
                                        </td>
                                        <td>
                                            <input id="shipto_company_field" name="shipto_company" size="30" value="" maxlength="64" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_first_name" for="shipto_first_name_field">
                                                First Name *						</label>
                                        </td>
                                        <td>
                                            <input required="required" aria-required="true" id="shipto_first_name_field" name="shipto_first_name" size="30" value="" class="required" maxlength="32" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_middle_name" for="shipto_middle_name_field">
                                                Middle Name						</label>
                                        </td>
                                        <td>
                                            <input id="shipto_middle_name_field" name="shipto_middle_name" size="30" value="" maxlength="32" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_last_name" for="shipto_last_name_field">
                                                Last Name *						</label>
                                        </td>
                                        <td>
                                            <input required="required" aria-required="true" id="shipto_last_name_field" name="shipto_last_name" size="30" value="" class="required" maxlength="32" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_address_1" for="shipto_address_1_field">
                                                Address 1 *						</label>
                                        </td>
                                        <td>
                                            <input required="required" aria-required="true" id="shipto_address_1_field" name="shipto_address_1" size="30" value="" class="required" maxlength="64" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_address_2" for="shipto_address_2_field">
                                                Address 2						</label>
                                        </td>
                                        <td>
                                            <input id="shipto_address_2_field" name="shipto_address_2" size="30" value="" maxlength="64" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_zip" for="shipto_zip_field">
                                                Zip / Postal Code *						</label>
                                        </td>
                                        <td>
                                            <input required="required" aria-required="true" id="shipto_zip_field" name="shipto_zip" size="30" value="" class="required" maxlength="32" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_city" for="shipto_city_field">
                                                City *						</label>
                                        </td>
                                        <td>
                                            <input required="required" aria-required="true" id="shipto_city_field" name="shipto_city" size="30" value="" class="required" maxlength="32" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_virtuemart_country_id" for="shipto_virtuemart_country_id_field">
                                                Country *						</label>
                                        </td>
                                        <td>
                                            <select required="required" aria-required="true" id="shipto_virtuemart_country_id" name="shipto_virtuemart_country_id" class="virtuemart_country_id required">
                        <option value="" selected="selected">-- Select --</option>
                        <option value="105">Italy</option>
                        <option value="222">United Kingdom</option>
                    </select>
                                        </td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_virtuemart_state_id" for="shipto_virtuemart_state_id_field">
                                                State / Province / Region *						</label>
                                        </td>
                                        <td>
                                            <select class="inputbox multiple" id="virtuemart_state_id" size="1" name="shipto_virtuemart_state_id" required="">
                                            <option value="">-- Select --</option>
                                            </select>					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_phone_1" for="shipto_phone_1_field">
                                                Phone						</label>
                                        </td>
                                        <td>
                                            <input id="shipto_phone_1_field" name="shipto_phone_1" size="30" value="" maxlength="32" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_phone_2" for="shipto_phone_2_field">
                                                Mobile Phone						</label>
                                        </td>
                                        <td>
                                            <input id="shipto_phone_2_field" name="shipto_phone_2" size="30" value="" maxlength="32" type="text"> 					</td>
                                    </tr>
                                        <tr>
                                        <td class="key" title="">
                                            <label class="shipto_fax" for="shipto_fax_field">
                                                Fax						</label>
                                        </td>
                                        <td>
                                            <input id="shipto_fax_field" name="shipto_fax" size="30" value="" maxlength="32" type="text"> 					</td>
                                    </tr>
                        
                                </tbody></table>
                            <? //</form>?>
        					</fieldset>
                            
                          <div id="btn_main_form_commands">
                            <button type="button" class="psbutton" id="btn_conferma_form_cancel">Annula</button>
                            <button type="button" class="psbutton" id="btn_conferma_form">Conferma</button>
                        </div>
        
<?	/*    
    <input type="hidden" name="option" value="com_virtuemart">
    <input type="hidden" name="view" value="user">
    <input type="hidden" name="controller" value="user">
    <input type="hidden" name="task" value="savecheckoutuser">
    <input type="hidden" name="layout" value="edit_address">
    <input type="hidden" name="address_type" value="BT">
    <input type="hidden" name="02303e8b08274756f6130cf15552b667" value="1">
	*/ ?>    
                        </div>
                        </article>
                      </div>
                    </div>
                </div>
            </div>
        </div>
<script language="javascript">
function myValidator(f, t) {
f.task.value = t; //this is a method to set the task of the form on the fTask.
if (document.formvalidator.isValid(f)) {
f.submit();
return true;
} else {
var msg = 'Required field is missing';
alert(msg + ' ');
}
return false;
}

function callValidatorForRegister(f) {

var elem = $('#username_field');
elem.attr('class', "required");

var elem = $('#name_field');
elem.attr('class', "required");

var elem = $('#password_field');
elem.attr('class', "required");

var elem = $('#password2_field');
elem.attr('class', "required");

var elem = $('#userForm');

return myValidator(f, 'registercheckoutuser');

}
</script>
<?
$allow_form=false;
if($allow_form){
	//require_once JPATH_COMPONENT.DS.'controllers'.DS.'user.php';
	//$vmUser = new VirtueMartControllerUser();
	//var_dump($vmUser);
	//$vmUser->editAddressCheckout();
	
	//$userFieldsModel = VmModel::getModel('UserFields');
	//$userFields = $userFieldsModel->getUserFields();
	//var_dump($vmUser->user);
	//$this->userFields = $userFieldsModel->getUserFieldsFilled($userFields, $this->user);
    // vmdebug('user edit address',$this->userFields['fields']);
    // Implement Joomla's form validation
    JHTML::_ ('behavior.formvalidation');
    JHTML::stylesheet ('vmpanels.css', JURI::root () . 'components/com_virtuemart/assets/css/');

    if ($this->fTask === 'savecartuser') {
        $rtask = 'registercartuser';
        $url = 0;
    }
    else {
        $rtask = 'registercheckoutuser';
        $url = JRoute::_ ('index.php?option=com_virtuemart&view=cart&task=checkout', $this->useXHTML, $this->useSSL);
    }
    ?>
    <h1><?php echo $this->page_title ?></h1>
    <?php
    echo shopFunctionsF::getLoginForm (TRUE, FALSE, $url);
    ?>
    <script language="javascript">
        function myValidator(f, t) {
            f.task.value = t; //this is a method to set the task of the form on the fTask.
            if (document.formvalidator.isValid(f)) {
                f.submit();
                return true;
            } else {
                var msg = '<?php echo addslashes (JText::_ ('COM_VIRTUEMART_USER_FORM_MISSING_REQUIRED_JS')); ?>';
                alert(msg + ' ');
            }
            return false;
        }

        function callValidatorForRegister(f) {

            var elem = $('#username_field');
            elem.attr('class', "required");

            var elem = $('#name_field');
            elem.attr('class', "required");

            var elem = $('#password_field');
            elem.attr('class', "required");

            var elem = $('#password2_field');
            elem.attr('class', "required");

            var elem = $('#userForm');

            return myValidator(f, '<?php echo $rtask ?>');

        }
    </script>

    <fieldset>
        <h2><?php
            if ($this->address_type == 'BT') {
                echo JText::_ ('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_LBL');
            }
            else {
                echo JText::_ ('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL');
            }
            ?>
        </h2>
    <?  /*
        <form method="post" id="userForm" name="userForm" class="form-validate">
            <!--<form method="post" id="userForm" name="userForm" action="<?php echo JRoute::_ ('index.php'); ?>" class="form-validate">-->*/ ?>
            <div class="control-buttons">
                <?php
                if (strpos ($this->fTask, 'cart') || strpos ($this->fTask, 'checkout')) {
                    $rview = 'cart';
                }
                else {
                    $rview = 'user';
                }
    // echo 'rview = '.$rview;

                if (strpos ($this->fTask, 'checkout') || $this->address_type == 'ST') {
                    $buttonclass = 'default';
                }
                else {
                    $buttonclass = 'button vm-button-correct';
                }


                if (VmConfig::get ('oncheckout_show_register', 1) && $this->userId == 0 && !VmConfig::get ('oncheckout_only_registered', 0) && $this->address_type == 'BT' and $rview == 'cart') {
                    echo JText::sprintf ('COM_VIRTUEMART_ONCHECKOUT_DEFAULT_TEXT_REGISTER', JText::_ ('COM_VIRTUEMART_REGISTER_AND_CHECKOUT'), JText::_ ('COM_VIRTUEMART_CHECKOUT_AS_GUEST'));
                }
                else {
                    //echo JText::_('COM_VIRTUEMART_REGISTER_ACCOUNT');
                }
                if (VmConfig::get ('oncheckout_show_register', 1) && $this->userId == 0 && $this->address_type == 'BT' and $rview == 'cart') {
                    ?>

                    <button class="<?php echo $buttonclass ?>" type="submit" onclick="javascript:return callValidatorForRegister(userForm);"
                            title="<?php echo JText::_ ('COM_VIRTUEMART_REGISTER_AND_CHECKOUT'); ?>"><?php echo JText::_ ('COM_VIRTUEMART_REGISTER_AND_CHECKOUT'); ?></button>
                    <?php if (!VmConfig::get ('oncheckout_only_registered', 0)) { ?>
                        <button class="<?php echo $buttonclass ?>" title="<?php echo JText::_ ('COM_VIRTUEMART_CHECKOUT_AS_GUEST'); ?>" type="submit"
                                onclick="javascript:return myValidator(userForm, '<?php echo $this->fTask; ?>');"><?php echo JText::_ ('COM_VIRTUEMART_CHECKOUT_AS_GUEST'); ?></button>
                        <?php } ?>
                    <button class="default" type="reset"
                            onclick="window.location.href='<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=' . $rview); ?>'"><?php echo JText::_ ('COM_VIRTUEMART_CANCEL'); ?></button>


                    <?php
                }
                else {
                    ?>

                    <button class="<?php echo $buttonclass ?>" type="submit"
                            onclick="javascript:return myValidator(userForm, '<?php echo $this->fTask; ?>');"><?php echo JText::_ ('COM_VIRTUEMART_SAVE'); ?></button>
                    <button class="default" type="reset"
                            onclick="window.location.href='<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=' . $rview); ?>'"><?php echo JText::_ ('COM_VIRTUEMART_CANCEL'); ?></button>

                    <?php } ?>
            </div>


            <?php
            if (!class_exists ('VirtueMartCart')) {
                require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');
            }

            if (count ($this->userFields['functions']) > 0) {
                echo '<script language="javascript">' . "\n";
                echo join ("\n", $this->userFields['functions']);
                echo '</script>' . "\n";
            }
            echo $this->loadTemplate ('userfields');

            ?>
    </fieldset>
    <?php // }
    //added by Taslima
		$type = JRequest::getVar('type');
		if($type == 'digital'){
	
		} 
		else{
			if ($this->userDetails->JUser->get ('id')) {
			echo $this->loadTemplate ('addshipto');
		}
    }
	/*
    //<input type="hidden" name="option" value="com_virtuemart"/>
    //<input type="hidden" name="view" value="user"/>
	//<input type="hidden" name="layout" value="<?php echo $this->getLayout (); ?>"/>
    */?>
    <input type="hidden" name="controller" value="user"/>
    <input type="hidden" name="task" value="<?php echo $this->fTask; // I remember, we removed that, but why?   ?>"/>
    <input type="hidden" name="address_type" value="<?php echo $this->address_type; ?>"/>
    <?php if (!empty($this->virtuemart_userinfo_id)) {
        echo '<input type="hidden" name="shipto_virtuemart_userinfo_id" value="' . (int)$this->virtuemart_userinfo_id . '" />';
    }
    echo JHTML::_ ('form.token');
//</form>
}?>


