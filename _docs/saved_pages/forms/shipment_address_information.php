<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add/Edit shipment address</title>
</head>
<body>
<h4><a href="http://localhost/+last-message/movebox3/index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[0]=0&lang=en">Add/Edit shipment address</a></h4>
<hr>
option=com_virtuemart<br>
view=user<br>
task=editaddresscart<br>
addrtype=ST<br>
virtuemart_user_id[0]=0<br>
<hr>
	<form method="post" id="userForm" name="userForm" class="form-validate">
		<div class="control-buttons">
            <button class="default" type="submit" onClick="javascript:return myValidator(userForm, 'savecartuser');">Save</button>
            <button class="default" type="reset" onClick="window.location.href='/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=cart&amp;lang=en'">Cancel</button>
		</div>
		<table class="adminForm user-details">
			<tbody>
            	<tr>
					<td class="key" title="">
						<label class="shipto_address_type_name" for="shipto_address_type_name_field">
							Address Nickname *						</label>
					</td>
					<td>
					  <input type="text" id="shipto_address_type_name_field" name="shipto_address_type_name" size="30" value="" class="required" maxlength="32" aria-required="true" required> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_company" for="shipto_company_field">
							Company Name						</label>
					</td>
					<td>
						<input type="text" id="shipto_company_field" name="shipto_company" size="30" value="" maxlength="64"> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_first_name" for="shipto_first_name_field">
							First Name *						</label>
					</td>
					<td>
					  <input type="text" id="shipto_first_name_field" name="shipto_first_name" size="30" value="" class="required" maxlength="32" aria-required="true" required> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_middle_name" for="shipto_middle_name_field">
							Middle Name						</label>
					</td>
					<td>
						<input type="text" id="shipto_middle_name_field" name="shipto_middle_name" size="30" value="" maxlength="32"> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_last_name" for="shipto_last_name_field">
							Last Name *						</label>
					</td>
					<td>
					  <input type="text" id="shipto_last_name_field" name="shipto_last_name" size="30" value="" class="required" maxlength="32" aria-required="true" required> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_address_1" for="shipto_address_1_field">
							Address 1 *						</label>
					</td>
					<td>
					  <input type="text" id="shipto_address_1_field" name="shipto_address_1" size="30" value="" class="required" maxlength="64" aria-required="true" required> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_address_2" for="shipto_address_2_field">
							Address 2						</label>
					</td>
					<td>
						<input type="text" id="shipto_address_2_field" name="shipto_address_2" size="30" value="" maxlength="64"> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_zip" for="shipto_zip_field">
							Zip / Postal Code *						</label>
					</td>
					<td>
					  <input type="text" id="shipto_zip_field" name="shipto_zip" size="30" value="" class="required" maxlength="32" aria-required="true" required> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_city" for="shipto_city_field">
							City *						</label>
					</td>
					<td>
					  <input type="text" id="shipto_city_field" name="shipto_city" size="30" value="" class="required" maxlength="32" aria-required="true" required> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_virtuemart_country_id" for="shipto_virtuemart_country_id_field">
							Country *						</label>
					</td>
					<td>
					  <select id="shipto_virtuemart_country_id" name="shipto_virtuemart_country_id" class="virtuemart_country_id required" aria-required="true" required="required">
	<option value="" selected="selected">-- Select --</option>
	<option value="105">Italy</option>
	<option value="222">United Kingdom</option>
</select>
					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_virtuemart_state_id" for="shipto_virtuemart_state_id_field">
							State / Province / Region *						</label>
					</td>
					<td>
					  <select class="inputbox multiple" id="virtuemart_state_id" size="1" name="shipto_virtuemart_state_id" required="">
						<option value="">-- Select --</option>
						</select>					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_phone_1" for="shipto_phone_1_field">
							Phone						</label>
					</td>
					<td>
						<input type="text" id="shipto_phone_1_field" name="shipto_phone_1" size="30" value="" maxlength="32"> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_phone_2" for="shipto_phone_2_field">
							Mobile Phone						</label>
					</td>
					<td>
						<input type="text" id="shipto_phone_2_field" name="shipto_phone_2" size="30" value="" maxlength="32"> 					</td>
				</tr>
            	<tr>
					<td class="key" title="">
						<label class="shipto_fax" for="shipto_fax_field">
							Fax						</label>
					</td>
					<td>
						<input type="text" id="shipto_fax_field" name="shipto_fax" size="30" value="" maxlength="32"> 					</td>
				</tr>
			</tbody>
		</table>
        
        <input type="hidden" name="option" value="com_virtuemart">
        <input type="hidden" name="view" value="user">
        <input type="hidden" name="controller" value="user">
        <input type="hidden" name="task" value="savecartuser">
        <input type="hidden" name="layout" value="edit_address">
        <input type="hidden" name="address_type" value="ST">
        <input type="hidden" name="aa6df98552e4ae287a606ece7afe1d73" value="1">            
	</form>
<script language="javascript">
function myValidator(f, t) {
	f.task.value = t; //this is a method to set the task of the form on the fTask.
	if (document.formvalidator.isValid(f)) {
		f.submit();
		return true;
	} else {
		var msg = 'Required field is missing';
		alert(msg + ' ');
	}
	return false;
}

function callValidatorForRegister(f) {

	var elem = jQuery('#username_field');
	elem.attr('class', "required");

	var elem = jQuery('#name_field');
	elem.attr('class', "required");

	var elem = jQuery('#password_field');
	elem.attr('class', "required");

	var elem = jQuery('#password2_field');
	elem.attr('class', "required");

	var elem = jQuery('#userForm');

	return myValidator(f, 'registercartuser');
}
</script>    
</body>
</html>