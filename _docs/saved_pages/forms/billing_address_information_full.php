﻿<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add/Edit billing address information FULL</title>
</head>
<body>
<h2>Add/Edit billing address information</h2>
	<form method="post" id="userForm" name="userForm" class="form-validate">
		<div class="control-buttons">
			Please use <strong>Register And Checkout</strong> to easily get access to your order history, or use <strong>Checkout as Guest</strong>
			<br>
            <h5><button class="default" type="submit" onClick="javascript:return callValidatorForRegister(userForm);" title="Register And Checkout">Register And Checkout</button>
			VirtueMartControllerUser::registercartuser()</h5>
            <h5><button class="default" title="Checkout as Guest" type="submit" onClick="javascript:return myValidator(userForm, 'savecheckoutuser');">Checkout as Guest</button>
			VirtueMartControllerUser::savecartuser()</h5>
            <button class="default" type="reset" onClick="window.location.href='/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=cart&amp;lang=en'">Cancel</button>
		</div>
		<fieldset>
			<span class="userfields_info">Shopper Information</span>
            <table class="adminForm user-details">
                <tbody>
                    <tr>
                        	<td class="key" title="">
                            <label class="email" for="email_field">
                                E-Mail *						</label>
                        	</td>
                        	<td>
                            <input type="text" id="email_field" name="email" size="30" value="" class="required" maxlength="100" aria-required="true" required> 					</td>
                    	</tr>
                    <tr>
                        	<td class="key" title="">
                            <label class="username" for="username_field">
                                Username						</label>
                        	</td>
                        	<td>
                            <input type="text" id="username_field" name="username" size="30" value="" maxlength="25"> 					</td>
                    	</tr>
                    <tr>
                        	<td class="key" title="">
                            <label class="name" for="name_field">
                                Displayed Name						</label>
                        	</td>
                        	<td>
                            <input type="text" id="name_field" name="name" size="30" value="" maxlength="25"> 					</td>
                    	</tr>
                    <tr>
                        	<td class="key" title="">
                            <label class="password" for="password_field">
                                Password						</label>
                        	</td>
                        	<td>
                            <input type="password" id="password_field" name="password" size="30" class="inputbox">
                        	</td>
                    	</tr>
                    <tr>
                        	<td class="key" title="">
                            <label class="password2" for="password2_field">
                                Confirm Password						</label>
                        	</td>
                        	<td>
                            <input type="password" id="password2_field" name="password2" size="30" class="inputbox">
                        	</td>
                    	</tr>
                    <tr>
                        	<td class="key" title="">
                            <label class="agreed" for="agreed_field">
                                I agree to the Terms of Service						</label>
                        	</td>
                        	<td>
                            <input type="checkbox" name="agreed" id="agreed_field" value="1">					
                        	</td>
                    	</tr>
                </tbody>
            </table>
		</fieldset>
        <fieldset>
			<span class="userfields_info">Bill To</span>
        	<table class="adminForm user-details">
            	<tbody>
                	<tr>
                    	<td class="key" title="">
                        <label class="company" for="company_field">
                            Company Name						</label>
                    	</td>
                    	<td>
                        <input type="text" id="company_field" name="company" size="30" value="" maxlength="64"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="title" for="title_field">
                            Title						</label>
                    	</td>
                    	<td>
                        <select id="title" name="title">
    <option value="Mr">Mr</option>
    <option value="Mrs">Mrs</option>
    </select>
                    	</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="first_name" for="first_name_field">
                            First Name *						</label>
                    	</td>
                    	<td>
                        <input type="text" id="first_name_field" name="first_name" size="30" value="" class="required" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="middle_name" for="middle_name_field">
                            Middle Name						</label>
                    	</td>
                    	<td>
                        <input type="text" id="middle_name_field" name="middle_name" size="30" value="" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="last_name" for="last_name_field">
                            Last Name *						</label>
                    	</td>
                    	<td>
                        <input type="text" id="last_name_field" name="last_name" size="30" value="" class="required" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="address_1" for="address_1_field">
                            Address 1 *						</label>
                    	</td>
                    	<td>
                        <input type="text" id="address_1_field" name="address_1" size="30" value="" class="required" maxlength="64"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="address_2" for="address_2_field">
                            Address 2						</label>
                    	</td>
                    	<td>
                        <input type="text" id="address_2_field" name="address_2" size="30" value="" maxlength="64"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="zip" for="zip_field">
                            Zip / Postal Code *						</label>
                    	</td>
                    	<td>
                        <input type="text" id="zip_field" name="zip" size="30" value="" class="required" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="city" for="city_field">
                            City *						</label>
                    	</td>
                    	<td>
                        <input type="text" id="city_field" name="city" size="30" value="" class="required" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="virtuemart_country_id" for="virtuemart_country_id_field">
                            Country *						</label>
                    	</td>
                    	<td>
                        <select id="virtuemart_country_id" name="virtuemart_country_id" class="virtuemart_country_id required">
    <option value="" selected="selected">-- Select --</option>
    <option value="105">Italy</option>
    <option value="222">United Kingdom</option>
    </select>
                    	</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="virtuemart_state_id" for="virtuemart_state_id_field">
                            State / Province / Region *						</label>
                    	</td>
                    	<td>
                        <select class="inputbox multiple" id="virtuemart_state_id" size="1" name="virtuemart_state_id" required="">
                        <option value="">-- Select --</option>
                        </select>					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="phone_1" for="phone_1_field">
                            Phone						</label>
                    	</td>
                    	<td>
                        <input type="text" id="phone_1_field" name="phone_1" size="30" value="" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="phone_2" for="phone_2_field">
                            Mobile Phone						</label>
                    	</td>
                    	<td>
                        <input type="text" id="phone_2_field" name="phone_2" size="30" value="" maxlength="32"> 					</td>
                	</tr>
                    <tr>
                    	<td class="key" title="">
                        <label class="fax" for="fax_field">
                            Fax						</label>
                    	</td>
                    	<td>
                        <input type="text" id="fax_field" name="fax" size="30" value="" maxlength="32"> 					</td>
                	</tr>
    
            </tbody>
            </table>
		</fieldset>
        <input type="hidden" name="option" value="com_virtuemart">
        <input type="hidden" name="view" value="user">
        <input type="hidden" name="controller" value="user">
        <input type="hidden" name="task" value="savecheckoutuser">
        <input type="hidden" name="layout" value="edit_address">
        <input type="hidden" name="address_type" value="BT">
        <input type="hidden" name="aa6df98552e4ae287a606ece7afe1d73" value="1">    
	</form>
<script language="javascript">
function myValidator(f, t) {
	f.task.value = t; //this is a method to set the task of the form on the fTask.
	if (document.formvalidator.isValid(f)) {
		f.submit();
		return true;
	} else {
		var msg = 'Required field is missing';
		alert(msg + ' ');
	}
	return false;
}

function callValidatorForRegister(f) {

	var elem = jQuery('#username_field');
	elem.attr('class', "required");

	var elem = jQuery('#name_field');
	elem.attr('class', "required");

	var elem = jQuery('#password_field');
	elem.attr('class', "required");

	var elem = jQuery('#password2_field');
	elem.attr('class', "required");

	var elem = jQuery('#userForm');

	return myValidator(f, 'registercheckoutuser');
}
</script>
</body>
</html>