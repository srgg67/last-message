<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add/Edit billing address information</title>
</head>
<body>
<h4>Add/Edit billing address information &nbsp;  <a href="http://localhost/+last-message/movebox3/index.php?option=com_virtuemart&view=user&task=editaddresscart&type=digital&addrtype=BT&lang=en">digital</a> &nbsp; | &nbsp; <a href="http://localhost/+last-message/movebox3/index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT&lang=en">real</a></h4>
<?	
// digital:
//http://localhost/+last-message/movebox3/index.php?option=com_virtuemart&view=user&task=editaddresscart&type=digital&addrtype=BT&lang=en
// real:
//http://localhost/+last-message/movebox3/index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT&lang=en

?>
<hr>
option=com_virtuemart<br>
view=user<br>
task=editaddresscart<br>
addrtype=BT<br>
---------------------<br>
if digital type:<br>
type=digital<br>
<hr>
	<form method="post" id="userForm" name="userForm" class="form-validate">
		<div class="control-buttons">
			Please use <strong>Register And Checkout</strong> to easily get access to your order history, or use <strong>Checkout as Guest</strong>
	  	  <button class="button vm-button-correct art-button" type="submit" onClick="javascript:return callValidatorForRegister(userForm);" title="Register And Checkout">Register And Checkout</button>
		  <button class="button vm-button-correct art-button" title="Checkout as Guest" type="submit" onClick="javascript:return myValidator(userForm, 'savecartuser');">Checkout as Guest</button>
			<button class="default" type="reset" onClick="window.location.href='/+last-message/movebox3/index.php?option=com_virtuemart&amp;view=cart&amp;lang=en'">Cancel</button>
	  </div>
        <fieldset>
            <span class="userfields_info">Shopper Information</span>
        </fieldset>
        <table class="adminForm user-details">
            <tbody>
                <tr>
                    <td class="key" title="">
                        <label class="email" for="email_field">
                            E-Mail *						</label>
                    </td>
                    <td>
                        <input type="text" id="email_field" name="email" size="30" value="" class="required" maxlength="100" aria-required="true" required> 					</td>
                </tr>
                <tr>
                    <td class="key" title="">
                        <label class="username" for="username_field">
                            Username						</label>
                    </td>
                    <td>
                        <input type="text" id="username_field" name="username" size="30" value="" maxlength="25"> 					</td>
                </tr>
                <tr>
                    <td class="key" title="">
                        <label class="name" for="name_field">
                            Displayed Name						</label>
                    </td>
                    <td>
                        <input type="text" id="name_field" name="name" size="30" value="" maxlength="25"> 					</td>
                </tr>
                <tr>
                    <td class="key" title="">
                        <label class="password" for="password_field">
                            Password						</label>
                    </td>
                    <td>
                        <input type="password" id="password_field" name="password" size="30" class="inputbox">
                    </td>
                </tr>
                <tr>
                    <td class="key" title="">
                        <label class="password2" for="password2_field">
                            Confirm Password						</label>
                    </td>
                    <td>
                        <input type="password" id="password2_field" name="password2" size="30" class="inputbox">
                    </td>
                </tr>
                <tr>
                    <td class="key" title="">
                        <label class="agreed" for="agreed_field">
                            I agree to the Terms of Service						</label>
                    </td>
                    <td>
                        <input type="checkbox" name="agreed" id="agreed_field" value="1">					</td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="option" value="com_virtuemart">
        <input type="hidden" name="view" value="user">
        <input type="hidden" name="controller" value="user">
        <input type="hidden" name="task" value="savecartuser">
        <input type="hidden" name="layout" value="edit_address">
        <input type="hidden" name="address_type" value="BT">
        <input type="hidden" name="aa6df98552e4ae287a606ece7afe1d73" value="1">
</form>
<script language="javascript">
function myValidator(f, t) {
	f.task.value = t; //this is a method to set the task of the form on the fTask.
	if (document.formvalidator.isValid(f)) {
		f.submit();
		return true;
	} else {
		var msg = 'Required field is missing';
		alert(msg + ' ');
	}
	return false;
}

function callValidatorForRegister(f) {

	var elem = jQuery('#username_field');
	elem.attr('class', "required");

	var elem = jQuery('#name_field');
	elem.attr('class', "required");

	var elem = jQuery('#password_field');
	elem.attr('class', "required");

	var elem = jQuery('#password2_field');
	elem.attr('class', "required");

	var elem = jQuery('#userForm');

	return myValidator(f, 'registercartuser');

}
</script>    
</body>
</html>